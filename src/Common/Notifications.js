import React,{useEffect} from 'react';
import Snackbar from '@material-ui/core/Snackbar';
import MuiAlert from '@material-ui/lab/Alert';
import { makeStyles } from '@material-ui/core/styles';

function Alert(props) {
  return <MuiAlert elevation={6} variant="filled" {...props} />;
}

const useStyles = makeStyles((theme) => ({
  root: {
    width: '100%',
    '& > * + *': {
      marginTop: theme.spacing(2),
    },
  },
}));
/**
 * Usage - takes 3 input
 * 1 - Message type ( success, error, warning, info ) | prop name - 'type'
 * 2 - Message to be displayed | prop name - 'message'
 * 3 - open or close | prop name - 'open'
 * @param {*} props 
 */
export default function Notifications(props) {
  const classes = useStyles();
//   const [open, setOpen] = React.useState(false);
  const { open, message, type } = props;

    const handleClose = () => {
        props.closeFn()
    }

  return (
    <div className={classes.root}>
      <Snackbar anchorOriginTopRight open={open} autoHideDuration={6000} onClose={handleClose}>
        <Alert onClose={handleClose} severity={type}>
          { message }
        </Alert>
      </Snackbar>
    </div>
  );
}
