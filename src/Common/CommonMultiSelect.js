import React, { Component } from 'react';
import { Multiselect } from 'multiselect-react-dropdown';

export default class CommonMultiSelect extends Component {
  constructor(props){
    super(props);
    this.state = {
      options:[{name: 'Srigar', id: 1},{name: 'Sam', id: 2},
      {name: 'Srigar1', id: 3},{name: 'Sam1', id: 4},
      {name: 'Srigar2', id: 5},{name: 'Sam2', id: 6},{name: 'Sam3', id: 7},{name: 'Srigar4', id: 8},{name: 'Sam5', id: 9}]
    }
  }
  onSelect(selectedList, selectedItem) {
    console.log(selectedItem);
}

  onRemove(selectedList, removedItem) {
    console.log(removedItem);
  }
  render() {
    return (
      <div>
        <Multiselect
        options={this.state.options} // Options to display in the dropdown
        selectedValues={this.state.selectedValue} // Preselected value to persist in dropdown
        onSelect={this.onSelect} // Function will trigger on select event
        onRemove={this.onRemove} // Function will trigger on remove event
        displayValue="name" // Property name to display in the dropdown options
        placeholder="Select a candidate"
        />
      </div>
    )
  }
}
