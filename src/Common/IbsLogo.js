import React from "react";
import ibs_Logo from "../assets/images/ibs_Logo.png";

/**
 * @author
 * @function IbsLogo
 **/

const IbsLogo = (props) => {
  return <img src={ibs_Logo} alt="Logo" />;
};

export default IbsLogo;
