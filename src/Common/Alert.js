import React from "react";
import Button from "@material-ui/core/Button";
import Dialog from "@material-ui/core/Dialog";
import DialogActions from "@material-ui/core/DialogActions";
import DialogContent from "@material-ui/core/DialogContent";
import DialogContentText from "@material-ui/core/DialogContentText";
import DialogTitle from "@material-ui/core/DialogTitle";
import { Snackbar } from "@material-ui/core";
import Alert from "@material-ui/lab/Alert";

export default function AlertDialog(props) {
  const title = "Confirmation";
  const { open, content, close } = props;
  return (
    // <div>
    //   <Dialog
    //     open={open}
    //     onClose={close}
    //     fullWidth={true}
    //     maxWidth={"sm"}
    //     aria-labelledby="alert-dialog-title"
    //     aria-describedby="alert-dialog-description"
    //   >
    //     <DialogTitle id="alert-dialog-title">{title}</DialogTitle>
    //     <DialogContent>
    //       <DialogContentText id="alert-dialog-description">
    //         {content}
    //       </DialogContentText>
    //     </DialogContent>
    //     <DialogActions>
    //       <Button onClick={close} color="primary">
    //         <b>OK</b>
    //       </Button>
    //     </DialogActions>
    //   </Dialog>
    // </div>
    <Snackbar open={open} autoHideDuration={6000} onClose={close}>
      <Alert
        elevation={6}
        variant="filled"
        onClose={close}
        severity={content.type}
      >
        {content.content}
      </Alert>
    </Snackbar>
  );
}
