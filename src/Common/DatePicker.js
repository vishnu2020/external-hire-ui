import React, { useState, useEffect, useRef } from 'react';
import { makeStyles } from '@material-ui/core/styles';
import TextField from '@material-ui/core/TextField';

const useStyles = makeStyles((theme) => ({
  container: {
    display: 'flex',
    flexWrap: 'wrap',
  },
  textField: {
    marginLeft: theme.spacing(1),
    marginRight: theme.spacing(1),
    width: 200,
  },
}));

function DatePickers(props, ref) {
  const classes = useStyles();

  const dateElem = useRef();

  const [currentDate, setDate] = useState('');

  const setDefaultDate = () => {
    var today = new Date();
    var dd = today.getDate();
    
    var mm = today.getMonth()+1; 
    var yyyy = today.getFullYear();
    if(dd<10) 
    {
        dd='0'+dd;
    } 
    
    if(mm<10) 
    {
        mm='0'+mm;
    } 

    today = `${yyyy}-${mm}-${dd}`
    console.log(today)
    return today+'';
  }

  const pickDate = (e) =>{
    e.preventDefault();
    props.onPickDate(e.target.value)
  }

  useEffect(() => {
    setDate(setDefaultDate);
    }, [])

  return (
    <form className={classes.container} noValidate>
      <TextField
        id="date"
        label="select date"
        type="date"
        defaultValue={currentDate}
        inputRef={ref}
        className={classes.textField}
        onChange={pickDate}
        InputLabelProps={{
          shrink: true,
        }}
      />
    </form>
  );
}

const forwardInput = React.forwardRef(DatePickers)

export default forwardInput
