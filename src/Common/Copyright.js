import React from "react";
import { Link, Typography } from "@material-ui/core";
export default function Copyright() {
  return (
    <Typography variant="body2" color="textSecondary" align="center">
      {"Copyright © "}
      <Link
        color="inherit"
        href="https://www.ibsplc.com/"
        style={{ color: "#00B4B4" }}
      >
        IBS Software
      </Link>{" "}
      <span style={{ color: "#004164" }}>{new Date().getFullYear()}</span>
      {"."}
    </Typography>
  );
}
