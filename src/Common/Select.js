import React from 'react';
import { makeStyles } from '@material-ui/core/styles';
import InputLabel from '@material-ui/core/InputLabel';
import MenuItem from '@material-ui/core/MenuItem';
import FormHelperText from '@material-ui/core/FormHelperText';
import FormControl from '@material-ui/core/FormControl';
import Select from '@material-ui/core/Select';

const useStyles = makeStyles((theme) => ({
  formControl: {
    width: "-webkit-fill-available"
  },
  selectEmpty: {
    marginTop: theme.spacing(2),
  },
}));

export default function SimpleSelect(props) {
  const classes = useStyles();
  const [recruiter, setRecruiter] = React.useState('');

  const handleChange = (event) => {
    setRecruiter(event.target.value)
    props.setRecruiter(props.data.filter( rec => rec.id === event.target.value)[0])
  };

  return (
      <FormControl required className={classes.formControl}>
        <InputLabel id="demo-simple-select-helper-label">Recruiter</InputLabel>
        <Select
          labelId="demo-simple-select-helper-label"
          id="demo-simple-select-helper"
          value={recruiter}
          onChange={handleChange}
        >
          <MenuItem value="">
            <em>None</em>
          </MenuItem>
          {
              props.data.map(select => <MenuItem name={select.employeeName} value={select.id}>{select.employeeName}</MenuItem>)
          }
        </Select>
        { props.error ? <FormHelperText style={{color:"red"}}>{props.errorMsg}</FormHelperText> : ""}
      </FormControl>
  );
}
