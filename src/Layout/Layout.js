import React, { useState } from "react";
import clsx from "clsx";
import { makeStyles } from "@material-ui/core/styles";
import CssBaseline from "@material-ui/core/CssBaseline";
import Drawer from "@material-ui/core/Drawer";
import Box from "@material-ui/core/Box";
import AppBar from "@material-ui/core/AppBar";
import Toolbar from "@material-ui/core/Toolbar";
import Typography from "@material-ui/core/Typography";
import IconButton from "@material-ui/core/IconButton";
import Container from "@material-ui/core/Container";
import MenuIcon from "@material-ui/icons/Menu";
import ChevronLeftIcon from "@material-ui/icons/ChevronLeft";
import { Route, Switch } from "react-router-dom";
import Copyright from "../Common/Copyright";
import Request from "../Components/JobRequest/Request";
import IbsLogo from "../Common/IbsLogo";
import CreateOpening from "../Components/JobOpenings/CreateOpening";
import ViewOpenings from "../Components/JobOpenings/ViewOpenings";
import JobAllocation from "../Components/JobOpenings/JobAllocation";
import SideNavMenuList from "../Components/SideNav/SideNavMenuList";
import History from "../Components/Demand/history/History";
import DemandTabs from "../Components/Demand/DemandTabs";
import Report from "../Components/Report/Report";
import CandidateMapping from '../Components/Demand/CandidateMapping';
import WaitingList from "../Components/AllRequests/WaitingList";
import ShortList from "../Components/AllRequests/ShortListed";
import SelectedCandidate from "../Components/AllRequests/SelectedCandidate";
import RejectedCandidate from "../Components/AllRequests/RejectedCandidate";
// import CommonMultiSelect from '../Common/CommonMultiSelect';

const drawerWidth = 250;

const useStyles = makeStyles((theme) => ({
  root: {
    display: "flex",
  },
  toolbar: {
    paddingRight: 24, // keep right padding when drawer closed
  },
  toolbarIcon: {
    display: "flex",
    alignItems: "center",
    justifySource: "flex-end",
    padding: "0 8px",
    ...theme.mixins.toolbar,
  },
  appBar: {
    background: "#00B4B4",
    zIndex: theme.zIndex.drawer + 1,
    transition: theme.transitions.create(["width", "margin"], {
      easing: theme.transitions.easing.sharp,
      duration: theme.transitions.duration.leavingScreen,
    }),
  },
  appBarShift: {
    marginLeft: drawerWidth,
    width: `calc(100% - ${drawerWidth}px)`,
    transition: theme.transitions.create(["width", "margin"], {
      easing: theme.transitions.easing.sharp,
      duration: theme.transitions.duration.enteringScreen,
    }),
  },
  menuButton: {
    marginRight: 36,
  },
  menuButtonHidden: {
    display: "none",
  },
  title: {
    flexGrow: 1,
  },
  drawerPaper: {
    position: "relative",
    whiteSpace: "nowrap",
    width: drawerWidth,
    transition: theme.transitions.create("width", {
      easing: theme.transitions.easing.sharp,
      duration: theme.transitions.duration.enteringScreen,
    }),
  },
  drawerPaperClose: {
    overflowX: "hidden",
    transition: theme.transitions.create("width", {
      easing: theme.transitions.easing.sharp,
      duration: theme.transitions.duration.leavingScreen,
    }),
    width: theme.spacing(7),
    [theme.breakpoints.up("sm")]: {
      width: theme.spacing(9),
    },
  },
  appBarSpacer: theme.mixins.toolbar,
  source: {
    flexGrow: 1,
    height: "100vh",
    overflow: "auto",
  },
  container: {
    paddingTop: theme.spacing(4),
    paddingBottom: theme.spacing(4),
  },
  paper: {
    padding: theme.spacing(2),
    display: "flex",
    overflow: "auto",
    flexDirection: "column",
  },
  fixedHeight: {
    height: 450,
  },
}));

export default function Layout() {
  const classes = useStyles();
  const [open, setOpen] = useState(true);
  const [source, setSource] = useState("All");
  const handleDrawerOpen = () => {
    setOpen(true);
  };
  const handleDrawerClose = () => {
    setOpen(false);
  };
  const handleSource = (item) => {
    setSource(item);
  };
  return (
    <div className={classes.root}>
      <CssBaseline />
      <AppBar
        position="absolute"
        className={clsx(classes.appBar, open && classes.appBarShift)}
      >
        <Toolbar className={classes.toolbar}>
          <IconButton
            edge="start"
            color="inherit"
            aria-label="open drawer"
            onClick={handleDrawerOpen}
            className={clsx(
              classes.menuButton,
              open && classes.menuButtonHidden
            )}
          >
            <MenuIcon />
          </IconButton>
          <Typography
            component="h1"
            variant="h6"
            color="inherit"
            noWrap
            className={classes.title}
          >
            Recruitment Portal
          </Typography>
        </Toolbar>
      </AppBar>
      <Drawer
        variant="permanent"
        classes={{
          paper: clsx(classes.drawerPaper, !open && classes.drawerPaperClose),
        }}
        open={open}
      >
        <div className={classes.toolbarIcon}>
          <IbsLogo width={"20%"} />
          <IconButton onClick={handleDrawerClose}>
            <ChevronLeftIcon />
          </IconButton>
        </div>
        {/* side menu*/}
        <SideNavMenuList selected={source} setSource={handleSource} />
      </Drawer>
      <main className={classes.source}>
        <div className={classes.appBarSpacer} />
        <Container maxWidth="xl" className={classes.container}>
          <Switch>
            <Route path="/request/waitingList">
              {/* <Request
                titleSource={{ source: "All Request", subSource: source }}
              /> */}
              <WaitingList/>
            </Route>
            <Route path="/request/shortList">
              <ShortList/>
            </Route>
            <Route path="/request/selected">
              <SelectedCandidate/>
            </Route>
            <Route path="/request/rejected">
              <RejectedCandidate/>
            </Route>
            <Route path="/request-careerPortal">
              <Request
                titleSource={{ source: "Career Portal", subSource: source }}
              />
            </Route>
            <Route path="/request-referalPortal">
              <Request
                titleSource={{ source: "Referal Portal", subSource: source }}
              />
            </Route>
            <Route exact path="/jobOpenings/CreateOpening">
              <CreateOpening
                titleSource={{ source: "Job Openings", subSource: source }}
              />
            </Route>
            <Route exact path="/jobOpenings/viewOpenings">
              <ViewOpenings
                titleSource={{ source: "Job Openings", subSource: source }}
              />
            </Route>
            <Route exact path="/jobOpenings/jobAllocation">
              <JobAllocation
                titleSource={{ source: "Job Openings", subSource: source }}
              />
            </Route>
            <Route path="/demand/openDemand">
              <DemandTabs />
            </Route>
            <Route path="/demand/history">
              <History/>
            </Route>
            <Route path="/demand/candidateMapping">
              <CandidateMapping/>
            </Route>
            <Route path="/reports/report1">
              <Report value={"Report 1"} />
            </Route>
            <Route path="/reports/report2">
              <Report value={"Report 2"} />
            </Route>
          </Switch>
          <Box pt={4}>
            <Copyright />
          </Box>
        </Container>
      </main>
    </div>
  );
}
