import { makeStyles } from "@material-ui/core/styles";
import Copyright from "../Common/Copyright";
import { Box, Grid, Button, Typography } from "@material-ui/core";
import React, { useState, useEffect } from "react";
import Paper from "@material-ui/core/Paper";
import ResumeData from "./ResumeData";
import IbsLogo from "../Common/IbsLogo";
import HomeIcon from "@material-ui/icons/Home";
import { Link } from "react-router-dom";
import AlertDialog from "../Common/Alert";

const useStyles = makeStyles((theme) => ({
  root: {
    flexGrow: 1,
    minWidth: 60,
    minHeight: 10,
  },
  table: {
    minWidth: 300,
  },
  toolbarIcon: {
    display: "flex",
    alignItems: "center",
    justifySource: "flex-end",
    padding: "0 8px",
    ...theme.mixins.toolbar,
  },
  root1: {
    display: "flex",
    "& > *": {
      margin: theme.spacing(1),
    },
  },
}));

const ReadMore = (props) => {
  const classes = useStyles();
  const { selectedJob } = props;
  const [openPopUp, setOpenPopUp] = useState(false);
  const [alertOpen, setAlertOpen] = useState(false);

  const handleAlertClose = () => {
    setAlertOpen(false);
  };

  const handleOpenApplyJob = () => {
    setOpenPopUp(true);
  };

  const handleCloseApplyJob = (applyStatus) => {
    setOpenPopUp(false);
    setAlertOpen(applyStatus);
  };

  return (
    <div className={classes.root}>
      <Grid container spacing={12}>
        <Grid item xs={1}></Grid>
        <Grid item xs={8}>
          <Typography
            className={classes.title}
            variant="h6"
            id="tableTitle"
            component="div"
          >
            {"External Hire > Job Description"}
          </Typography>
        </Grid>

        <Grid item xs={2}></Grid>
        <Grid item xs={1}>
          <Link to="/">
            <HomeIcon />
          </Link>
        </Grid>
      </Grid>

      <Grid container spacing={12}>
        <Grid item xs={1}></Grid>
        <Grid item xs={10}>
          <h1>UI Architect</h1>
          <h2>Job Id: {selectedJob.jobId}</h2>
          <h2>Job location: {selectedJob.jobLocation}</h2>

          <h2>Job Description:</h2>
          <p>
            Work with clients and Project Managers/Architects to build web based
            applications
            <br />
            Determine appropriate architecture, other technical solutions, and
            make relevant recommendations to clients
            <br />
            Requirement analysis, impact analysis and estimation of change
            request
            <br />
            Prepare and conduct hand over sessions to other developers
            <br />
            Contributes to continual improvement by suggesting improvements to
            user interface, software architecture or new technologies
            <br />
            Alert colleagues on emerging technologies or applications and the
            opportunities to integrate them into existing stack
            <br />
            Candidate must have an experience in owning the design/architecture
            of complex product/project
            <br />
            Should have experience in UI NFR testing
            <br />
          </p>

          <h2>Key Skills:</h2>
          <p>
            Highly proficient in object oriented JavaScript, jQuery, HTML, CSS3,
            JSP and JSTL
            <br />
            Experience with React, AngularJS, responsive web design, Bootstrap
            (or similar CSS framework)
            <br />
            Adept at maintaining cross-browser compatibility and debugging
            <br />
            Knowledge/experience of User Interfaces and Interaction Design
          </p>

          <h2>Company Statement:</h2>

          <h2>Salary and Benefits:</h2>
          <p>
            <h2>Work Permit:</h2>
            <Button
              variant="contained"
              style={{ width: "140px", height: "50px" }}
              onClick={handleOpenApplyJob}
              style={{ background: "#85adad", color: "#fff" }}
            >
              Apply Here
            </Button>
            <br />
            You could also apply in confidence by writing to:
            <br />
            Manager - HR
            <br />
            IBS Software (P) Ltd.
            <br />
            Nila, Technopark Campus
            <br />
            Trivandrum - 695 581
            <br />
            Kerala
            <br />
            Ph: +91 0471 6614796 / 4763
            <br />
            Email: careers@ibsplc.com
            <br />
            <br />
            <br />
          </p>
          <h3>
            *Freshers Recruitment : Currently through Campus placement only.
          </h3>
          <Paper>
            {openPopUp ? (
              <ResumeData
                open={openPopUp}
                handleClose={handleCloseApplyJob}
                selectedJob={selectedJob}
              />
            ) : null}
          </Paper>
        </Grid>
      </Grid>
      {alertOpen ? (
        <AlertDialog
          open={alertOpen}
          content={{ content: "Applied", type: "success" }}
          close={handleAlertClose}
        />
      ) : null}
      
    </div>
  );
};

export default ReadMore;
