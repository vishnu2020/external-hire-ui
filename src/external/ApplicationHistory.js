import React, { useEffect, useState } from "react";
import {
  Dialog,
  Typography,
  withStyles,
  IconButton,
  makeStyles,
  Paper,
  DialogActions,
  TextField,
  InputAdornment,
  Grid,
  Button,
} from "@material-ui/core";
import MuiDialogTitle from "@material-ui/core/DialogTitle";
import MuiDialogContent from "@material-ui/core/DialogContent";
import CloseIcon from "@material-ui/icons/Close";
import SearchIcon from "@material-ui/icons/Search";
import Axios from "axios";
import Table from "@material-ui/core/Table";
import TableBody from "@material-ui/core/TableBody";
import TableCell from "@material-ui/core/TableCell";
import TableContainer from "@material-ui/core/TableContainer";
import TableHead from "@material-ui/core/TableHead";
import TablePagination from "@material-ui/core/TablePagination";
import TableRow from "@material-ui/core/TableRow";
import TableSortLabel from "@material-ui/core/TableSortLabel";

/**
 * @author
 * @function ApplicationHistory
 **/

const styles = (theme) => ({
  root: {
    margin: 0,
    padding: theme.spacing(2),
  },
  closeButton: {
    position: "absolute",
    right: theme.spacing(1),
    top: theme.spacing(1),
    color: theme.palette.grey[500],
  },
});

const DialogTitle = withStyles(styles)((props) => {
  const { children, classes, onClose, ...other } = props;
  const [searchText, setSearchText] = useState("");

  const handleChangeSearchText = (event) => {
    setSearchText(event.target.value);
  };
  return (
    <MuiDialogTitle disableTypography className={classes.root} {...other}>
      <Grid
        container
        direction="row"
        justify="center"
        alignItems="flex-end"
        spacing={3}
      >
        <Grid item xs={4}>
          <Typography variant="h4">{children}</Typography>
        </Grid>
        <Grid item xs={2}></Grid>
        <Grid item xs={3} style={{ height: "80%" }}>
          <TextField
            id="outlined-basic"
            placeholder="Search"
            size="small"
            value={searchText}
            onChange={handleChangeSearchText}
            style={{ width: "100%" }}
            InputProps={{
              startAdornment: (
                <InputAdornment position="start">
                  <SearchIcon />
                </InputAdornment>
              ),
            }}
          />
        </Grid>
        <Grid item xs={2}>
          <Button
            variant="contained"
            onClick={() => props.handleChangeSearchText(searchText)}
            style={{ background: "#85adad", color: "#fff" }}
          >
            Search
          </Button>
        </Grid>
        <Grid item xs={1}>
          {onClose ? (
            <IconButton
              color="secondary"
              aria-label="close"
              className={classes.closeButton}
              onClick={onClose}
            >
              <CloseIcon style={{ color: "red" }} />
            </IconButton>
          ) : null}
        </Grid>
      </Grid>
    </MuiDialogTitle>
  );
});

const DialogContent = withStyles((theme) => ({
  root: {
    padding: theme.spacing(2),
  },
}))(MuiDialogContent);

const headCells = [
  { id: "id", label: "ID", width: "short" },
  { id: "jobTitle", label: "Job Title", width: "long" },
  { id: "jobLocation", label: "Job Location", width: "long" },
  { id: "createdOn", label: "Applied On", width: "short" },
];

const useStyles = makeStyles((theme) => ({
  root: {
    width: "100%",
  },
  paper: {
    width: "100%",
    marginBottom: theme.spacing(2),
  },
  table: {
    minWidth: 750,
  },
  tablehead: {
    background: "#85adad",
    color: "#fff",
    fontSize: "120%",
  },
  container: {
    maxHeight: 400,
  },
  visuallyHidden: {
    border: 0,
    clip: "rect(0 0 0 0)",
    height: 1,
    margin: -1,
    overflow: "hidden",
    padding: 0,
    position: "absolute",
    top: 20,
    width: 1,
  },
  value: {
    paddingRight: theme.spacing(6),
  },
}));

const ApplicationHistory = (props) => {
  const classes = useStyles();
  const [jobOpenings, setJobOpenings] = useState([]);
  const [order, setOrder] = useState("asc");
  const [orderBy, setOrderBy] = useState("calories");
  const [page, setPage] = useState(0);
  const [rowsPerPage, setRowsPerPage] = useState(5);
  const [searchText, setSearchText] = useState("");

  const handleChangeSearchText = (value) => {
    setSearchText(value);
  };

  const handleSort = (property) => (event) => {
    const isAsc = orderBy === property && order === "asc";
    setOrder(isAsc ? "desc" : "asc");
    setOrderBy(property);
  };

  const handleChangePage = (event, newPage) => {
    setPage(newPage);
  };

  const handleChangeRowsPerPage = (event) => {
    setRowsPerPage(parseInt(event.target.value, 10));
    setPage(0);
  };

  const descComparator = (a, b, orderBy) => {
    if (b[orderBy] < a[orderBy]) {
      return -1;
    }
    if (b[orderBy] > a[orderBy]) {
      return 1;
    }
    return 0;
  };

  const stableSort = (array) => {
    const stabilizedThis = array.map((el, index) => [el, index]);
    stabilizedThis.sort((a, b) => {
      const orderVal =
        order === "desc"
          ? descComparator(a[0], b[0], orderBy)
          : -descComparator(a[0], b[0], orderBy);
      if (orderVal !== 0) return orderVal;
      return a[1] - b[1];
    });
    return stabilizedThis.map((el) => el[0]);
  };

  const emptyRows =
    rowsPerPage -
    Math.min(rowsPerPage, jobOpenings.length - page * rowsPerPage);
  const rowsToDisplay = stableSort(jobOpenings).slice(
    page * rowsPerPage,
    page * rowsPerPage + rowsPerPage
  );

  useEffect(() => {
    Axios.get(`http://localhost:8080/getAllJobOpenings`).then(
      (res) => {
        setJobOpenings(
          res.data.filter((data) => {
            if (
              data.jobTitle.toLowerCase().includes(searchText.toLowerCase()) ||
              data.jobLocation.toLowerCase().includes(searchText.toLowerCase())
            ) {
              return true;
            }
            return false;
          })
        );
        setPage(0);
      },
      (err) => {
        console.log(err);
      }
    );
  }, [searchText]);

  const jobTable = () => {
    return (
      <Paper className={classes.paper}>
        <TableContainer className={classes.container}>
          <Table
            className={classes.table}
            stickyHeader
            aria-labelledby="tableTitle"
            size="medium"
            aria-label="enhanced table"
          >
            <TableHead>
              <TableRow tabIndex={-1}>
                {headCells.map((headCell) => (
                  <TableCell
                    className={classes.tablehead}
                    key={headCell.id}
                    align="center"
                    sortDirection={orderBy === headCell.id ? order : false}
                    onClick={handleSort(headCell.id)}
                  >
                    {headCell.label}
                    {headCell.label === "" ? null : (
                      <TableSortLabel
                        active={orderBy === headCell.id}
                        direction={orderBy === headCell.id ? order : "asc"}
                      >
                        {orderBy === headCell.id ? (
                          <span className={classes.visuallyHidden}>
                            {order === "desc"
                              ? "sorted descending"
                              : "sorted ascending"}
                          </span>
                        ) : null}
                      </TableSortLabel>
                    )}
                  </TableCell>
                ))}
              </TableRow>
            </TableHead>
            <TableBody>
              {rowsToDisplay.map((row, index) => {
                return (
                  <TableRow hover role="checkbox" tabIndex={-1} key={row.id}>
                    {headCells.map((headCell, idx) => {
                      return (
                        <TableCell
                          className={classes.value}
                          key={idx}
                          align="center"
                        >
                          {headCell.id === "id"
                            ? index + 1
                            : headCell.id === "createdOn"
                            ? row[headCell.id].split("T")[0]
                            : row[headCell.id]}
                        </TableCell>
                      );
                    })}
                  </TableRow>
                );
              })}
              {emptyRows > 0 && (
                <TableRow style={{ height: 53 * emptyRows }}>
                  <TableCell colSpan={6} />
                </TableRow>
              )}
            </TableBody>
          </Table>
        </TableContainer>
      </Paper>
    );
  };

  return (
    <Dialog
      maxWidth="md"
      fullWidth={true}
      open={props.open}
      onClose={props.handleClose}
      aria-labelledby="max-width-dialog-title"
    >
      <DialogTitle
        id="customized-dialog-title"
        onClose={props.handleClose}
        handleChangeSearchText={handleChangeSearchText}
      >
        Applied Job(s)
      </DialogTitle>
      <DialogContent dividers>{jobTable()}</DialogContent>
      <DialogActions>
        <TablePagination
          rowsPerPageOptions={[3, 5, 10, 25]}
          component="div"
          count={jobOpenings.length}
          rowsPerPage={rowsPerPage}
          page={page}
          onChangePage={handleChangePage}
          onChangeRowsPerPage={handleChangeRowsPerPage}
        />
      </DialogActions>
    </Dialog>
  );
};

export default ApplicationHistory;
