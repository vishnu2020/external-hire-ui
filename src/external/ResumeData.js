import React, { useState, useEffect } from "react";
import { makeStyles } from "@material-ui/core/styles";
import Button from "@material-ui/core/Button";
import Dialog from "@material-ui/core/Dialog";
import DialogActions from "@material-ui/core/DialogActions";
import DialogContent from "@material-ui/core/DialogContent";
import DialogTitle from "@material-ui/core/DialogTitle";
import Axios from "axios";

import {
  Grid,
  TextField,
  Typography,
  Paper,
  Toolbar,
  FormControlLabel,
  RadioGroup,
  FormLabel,
  FormControl,
  Radio,
  Checkbox,
  Chip,
  Input,
  MenuItem,
} from "@material-ui/core";

import clsx from "clsx";
import { SERVER_URL } from "../Common/StaticContent";
import { CloudUploadOutlined } from "@material-ui/icons";
import Autocomplete from "@material-ui/lab/Autocomplete";

const useToolbarStyles = makeStyles((theme) => ({
  root: {
    paddingLeft: theme.spacing(2),
    paddingRight: theme.spacing(1),
  },
  title: {
    flex: "1 1 100%",
  },
  paper: {
    padding: theme.spacing(2),
    textAlign: "center",
    color: theme.palette.text.secondary,
    height: "60px",
  },
}));

const EnhancedTableToolbar = (props) => {
  const classes = useToolbarStyles();
  return (
    <Toolbar className={clsx(classes.root)}>
      <Grid
        container
        direction="row"
        justify="center"
        alignItems="center"
        spacing={3}
      >
        <center>
          <Grid item xs={12}>
            <Typography
              className={classes.title}
              variant="h6"
              id="tableTitle"
              component="div"
            >
              <Paper
                style={{ background: "#85adad", color: "#fff", width: "105%" }}
              >
                {"ExternalHire > Job Description > Apply Online"}
              </Paper>
            </Typography>
          </Grid>
        </center>

        <Grid item xs={1}></Grid>
      </Grid>
    </Toolbar>
  );
};

const useStyles = makeStyles((theme) => ({
  root: {
    flexGrow: 1,
  },
  paper: {
    padding: theme.spacing(2),
    textAlign: "center",
    color: theme.palette.text.secondary,
    height: "80px",
  },
  errorpaper: {
    padding: theme.spacing(2),
    textAlign: "center",
    color: theme.palette.text.secondary,
    height: "80px",
    background: "#ffcccc",
  },
  button: {
    textAlign: "center",
    padding: theme.spacing(1),
  },
}));

const ResumeData = (props) => {
  const { selectedJob } = props;
  console.log(selectedJob);
  const classes = useStyles();
  const [firstName, setFirstName] = useState("");
  const [middleName, setMiddleName] = useState("");
  const [lastName, setLastName] = useState("");
  const [emailId, setEmailId] = useState("");
  const [phoneNumber, setPhoneNumber] = useState("");
  const [gender, setGender] = useState("female");
  const [preferredLocation, setPreferredLocation] = useState([]);
  const [experience, setExperience] = useState("Select");
  const [skills, setSkills] = useState([]);
  const [currentOrganization, setOrganization] = useState("Select");
  const [validVisa, setValidVisa] = useState("Select");
  const [qualification, setQualification] = useState("Select");
  const [allowStoring, setAllowStoring] = useState(false);
  const [allowSubscription, setAllowSubcription] = useState(false);
  const [file, setFile] =useState(null);
  const [resume, setResume] =useState(null);
  const [jobLocationList, setJobLocationList] = useState(
    selectedJob.jobLocation.split(",")
  );
  const [skillList, setSkillList] = useState([
    "Java",
    "Python",
    "React",
    "MySql",
  ]);
  const [organizationList, setOrganizationList] = useState([
    "Select",
    "None",
    "TCS",
    "CTS",
    "Wipro",
    "IBM",
  ]);
  const [visaList, setVisaList] = useState([
    "Select",
    "None",
    "Type 1",
    "Type 2",
    "Type 3",
  ]);
  const [qualificationList, setQualificationList] = useState([
    "Select",
    "10th",
    "12th",
    "Graduate",
    "Masters",
  ]);

  const [fieldError, setFieldError] = useState({
    firstName: false,
    lastName: false,
    emailId: false,
    phoneNumber: false,
    preferredLocation: false,
    experience: false,
    skills: false,
    currentOrganization: false,
    validVisa: false,
    qualification: false,
  });
   useEffect(() => {},[]);

  const handleChangeFile = (event) => {
    setFile(event.target.files[0]);
  };



  // const handleChangeResume = (event, selected) => {
  //   setResume(event.target.files[0]);
  // };


  const handleChangeFirstName = (event) => {
    setFirstName(event.target.value);
    setFieldError({ ...fieldError, firstName: event.target.value === "" });
  };
  const handleChangeMiddleName = (event) => {
    setMiddleName(event.target.value);
  };
  const handleChangeLastName = (event) => {
    setLastName(event.target.value);
    setFieldError({ ...fieldError, lastName: event.target.value === "" });
  };
  const handleChangeEmailId = (event) => {
    setEmailId(event.target.value);
    setFieldError({
      ...fieldError,
      emailId: !/^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,3})+$/.test(
        event.target.value
      ),
    });
  };
  const handleChangePhoneNumber = (event, content) => {
    setPhoneNumber(event.target.value);
    setFieldError({
      ...fieldError,
      phoneNumber: !event.target.value.match(/^\d{10}$/),
    });
  };

  const handleChangeGender = (event) => {
    setGender(event.target.value);
  };

  const handleChangePreferredLocation = (event, loc) => {
    setPreferredLocation(loc);
    setFieldError({
      ...fieldError,
      preferredLocation: loc.length === 0,
    });
  };

  const handleChangeExpierence = (event) => {
    setExperience(event.target.value);
    setFieldError({
      ...fieldError,
      experience: event.target.value === "Select",
    });
  };

  const handleChangeSkills = (event, skill) => {
    setSkills(skill);
    setFieldError({
      ...fieldError,
      skills: skill.length === 0,
    });
  };

  const handleChangeCurrentOrganization = (event) => {
    setOrganization(event.target.value);
    setFieldError({
      ...fieldError,
      currentOrganization: event.target.value === "Select",
    });
  };

  const handleChangeValidVisa = (event) => {
    setValidVisa(event.target.value);
    setFieldError({
      ...fieldError,
      validVisa: event.target.value === "Select",
    });
  };

  const handleChangeQualification = (event) => {
    setQualification(event.target.value);
    setFieldError({
      ...fieldError,
      qualification: event.target.value === "Select",
    });
  };

  const handleChangeAllowStoring = (event) => {
    setAllowStoring(event.target.checked);
  };

  const handleChangeAllowSubcription = (event) => {
    setAllowSubcription(event.target.checked);
  };

  const arrayToString = (array) => {
    if (array.length === 0) {
      return "";
    }
    let str = "";
    for (let index = 0; index < array.length - 1; index++) {
      str = str + array[index] + ",";
    }
    return str + array[array.length - 1];
  };


  const onClickSubmit = () => {
    var formData = new FormData();
    setFieldError({
      firstName: firstName === null || firstName === "",
      lastName: lastName === null || lastName === "",
      emailId: emailId === null || emailId === "",
      phoneNumber: phoneNumber === null || phoneNumber === "",
      preferredLocation: preferredLocation.length === 0,
      experience: experience === "Select",
      skills: skills.length === 0,
      currentOrganization: currentOrganization === "Select",
      validVisa: validVisa === "Select",
      qualification: qualification === "Select",
    });
    if (
      firstName === null ||
      firstName === "" ||
      lastName === null ||
      lastName === "" ||
      emailId === null ||
      emailId === "" ||
      phoneNumber === null ||
      phoneNumber === "" ||
      preferredLocation.length === 0 ||
      experience === "Select" ||
      skills.length === 0 ||
      currentOrganization === "Select" ||
      validVisa === "Select" ||
      qualification === "Select"
    ) {
      return;
    } else {

      formData.append("jobId" , selectedJob.jobId)
      formData.append("firstName" , firstName)
      formData.append("middleName" , middleName)
      formData.append("lastName", lastName)
      formData.append("emailId" ,emailId)
      formData.append("phoneNumber",phoneNumber)
      formData.append("gender" , gender)
      formData.append("preferredLocation" ,arrayToString(preferredLocation))
      formData.append("experience" , experience)
      formData.append("skills" , arrayToString(skills))
      formData.append("currentOrganization" , currentOrganization)
      formData.append("validVisa" , validVisa)
      formData.append("qualification" , qualification)
      formData.append("allowStoring" , allowStoring)
      formData.append("allowSubscription" , allowSubscription)
      formData.append("source" , "Career Portal")
      formData.append("resume" , file)

      let newApply = {
        jobId: selectedJob.jobId,
        firstName: firstName,
        middleName: middleName,
        lastName: lastName,
        emailId: emailId,
        phoneNumber: phoneNumber,
        gender: gender,
        preferredLocation: arrayToString(preferredLocation),
        experience: experience,
        skills: arrayToString(skills),
        currentOrganization: currentOrganization,
        validVisa: validVisa,
        qualification: qualification,
        allowStoring: allowStoring,
        allowSubscription: allowSubscription,
        source: "Career Portal",
        resume: file,
        
        
      };
      console.log("Create With:", newApply);
      saveToDb(formData);
      // props.handleClose(true);
    }
  };

  const saveToDb = (object) => {
    var formData = new FormData();
    formData.append("enctype", 'multipart/form-data')
    // formData.append('resume', file )
    console.log(file);
  
   let keys = Object.keys(object);
   console.log(keys);

  //  for(let key in keys){
  //    formData.append(key, keys[key])
  //  }
  
// keys.forEach(obj =>
//   {
//     let key=obj;
//     let value= object[obj];
//     formData.append(key + "" , value)
//   console.log(key)
//   } )

  for ( var key in object ) {
    formData.append(key, object[key]);
}
   console.log(formData);
    //formData.append('candidate', object)
    Axios.post(`http://localhost:8080/saveCandidate`, object , {
      headers: {
        'Content-Type': 'multipart/form-data'
      }
    }).then(
      (res) => {
        console.log(res);
        props.handleClose(true);
      },
      (err) => {
        console.log(err);
        props.handleClose(false);
      }
    );
  };

  return (
    <Dialog
      fullWidth={true}
      maxWidth="lg"
      open={props.open}
      onClose={() => props.handleClose(false)}
      aria-labelledby="max-width-dialog-title"
    >
      <DialogTitle id="max-width-dialog-title">
        <EnhancedTableToolbar />
      </DialogTitle>
      <DialogContent>
        <div className={classes.root}>
          <Grid container spacing={3}>
            <Grid item xs={12} style={{ textAlign: "right" }}>
              {/* <Button variant="outlined" component="label">
                <CloudUploadOutlined />
                <div style={{ paddingLeft: "5px" }}>Upload Resume</div>
                <input
                  type="file"
                  style={{ display: "none" }}
                  value={resume}
                  onChange={handleChangeResume}
                />
              </Button> */}
              {/* <form> */}
                  <input type="file"  onChange={handleChangeFile}></input>
              {/* </form> */}
            </Grid>
            <Grid item xs={4}>
              <Paper className={classes.paper}>
                <TextField
                  error={fieldError["firstName"]}
                  id="outlined-basic"
                  label={
                    fieldError["firstName"]
                      ? "*Required Field - First Name"
                      : "First Name"
                  }
                  value={firstName}
                  onChange={handleChangeFirstName}
                  style={{ width: "100%" }}
                />
                {/* <div className="errorMsg">{errors.firstName}</div> */}
              </Paper>
            </Grid>
            <Grid item xs={4}>
              <Paper className={classes.paper}>
                <TextField
                  id="outlined-basic"
                  label={"Middle Name"}
                  value={middleName}
                  onChange={handleChangeMiddleName}
                  style={{ width: "100%" }}
                />
              </Paper>
            </Grid>
            <Grid item xs={4}>
              <Paper className={classes.paper}>
                <TextField
                  error={fieldError["lastName"]}
                  id="outlined-basic"
                  label={
                    fieldError["lastName"]
                      ? "*Required Field - Last Name"
                      : "Last Name"
                  }
                  value={lastName}
                  onChange={handleChangeLastName}
                  style={{ width: "100%" }}
                />
                {/* <div className="errorMsg">{errors.lastName}</div> */}
              </Paper>
            </Grid>
            <Grid item xs={5}>
              <Paper className={classes.paper}>
                <TextField
                  error={fieldError["emailId"]}
                  id="outlined-basic"
                  label={
                    fieldError["emailId"]
                      ? "*Required/Invalid - Email"
                      : "Email Id"
                  }
                  value={emailId}
                  onChange={handleChangeEmailId}
                  style={{ width: "100%" }}
                />
                {/* <div className="errorMsg">{errors.emailid}</div> */}
              </Paper>
            </Grid>
            <Grid item xs={3}>
              <Paper className={classes.paper}>
                <TextField
                  error={fieldError["phoneNumber"]}
                  id="outlined-basic"
                  label={
                    fieldError["phoneNumber"]
                      ? "*Required/Invalid - Number"
                      : "Contact Number"
                  }
                  value={phoneNumber}
                  onChange={handleChangePhoneNumber}
                  style={{ width: "100%" }}
                />
                {/* <div className="errorMsg">{errors.mobileno}</div> */}
              </Paper>
            </Grid>
            <Grid item xs={4}>
              <Paper className={classes.paper}>
                <FormControl component="fieldset">
                  <Grid container spacing={5}>
                    <Grid item xs={2}>
                      <FormLabel
                        component="legend"
                        style={{ paddingTop: "17px" }}
                      >
                        Gender
                      </FormLabel>
                    </Grid>
                    <Grid item xs={2}></Grid>
                    <Grid item xs={8}>
                      <RadioGroup
                        aria-label="gender"
                        name="gender"
                        value={gender}
                        onChange={handleChangeGender}
                        style={{ height: "35px", paddingTop: "5px" }}
                      >
                        <FormControlLabel
                          value="female"
                          control={<Radio />}
                          label="Female"
                        />
                        <FormControlLabel
                          value="male"
                          control={<Radio />}
                          label="Male"
                        />
                      </RadioGroup>
                    </Grid>
                  </Grid>
                </FormControl>
              </Paper>
            </Grid>
            <Grid item xs={4}>
              <Paper className={classes.paper}>
                <Autocomplete
                  multiple
                  options={jobLocationList}
                  value={preferredLocation}
                  id="standard-select-preferredLocation"
                  onChange={handleChangePreferredLocation}
                  renderInput={(params) => (
                    <TextField
                      {...params}
                      label={
                        fieldError["preferredLocation"]
                          ? "*Required Field - Preferred Location"
                          : "Preferred Location"
                      }
                      error={fieldError["preferredLocation"]}
                    />
                  )}
                />
              </Paper>
            </Grid>
            <Grid item xs={4}>
              <Paper className={classes.paper}>
                <TextField
                  error={fieldError["experience"]}
                  id="standard-select-experience"
                  select
                  label={
                    fieldError["experience"] ? "*Required Field" : "Experience"
                  }
                  value={experience}
                  onChange={handleChangeExpierence}
                  style={{ width: "100%" }}
                >
                  {["Select", "1", "2", "3", "4"].map((option, index) => (
                    <MenuItem key={index} value={option}>
                      {option}
                    </MenuItem>
                  ))}
                </TextField>
              </Paper>
            </Grid>
            <Grid item xs={4}>
              <Paper className={classes.paper}>
                <Autocomplete
                  multiple
                  options={skillList}
                  value={skills}
                  id="standard-select-skills"
                  onChange={handleChangeSkills}
                  renderInput={(params) => (
                    <TextField
                      {...params}
                      label={
                        fieldError["skills"]
                          ? "*Required Field - Skills"
                          : "Skills"
                      }
                      error={fieldError["skills"]}
                    />
                  )}
                />
                {/* <div className="errorMsg">{errors.skills}</div> */}
              </Paper>
            </Grid>
            <Grid item xs={4}>
              <Paper className={classes.paper}>
                <TextField
                  error={fieldError["currentOrganization"]}
                  id="standard-select-currentOrganization"
                  select
                  label={
                    fieldError["currentOrganization"]
                      ? "*Required Field"
                      : "Current Organization"
                  }
                  value={currentOrganization}
                  onChange={handleChangeCurrentOrganization}
                  style={{ width: "100%" }}
                >
                  {organizationList.map((option, index) => (
                    <MenuItem key={index} value={option}>
                      {option}
                    </MenuItem>
                  ))}
                </TextField>
              </Paper>
            </Grid>
            <Grid item xs={4}>
              <Paper className={classes.paper}>
                <TextField
                  error={fieldError["validVisa"]}
                  id="standard-select-validVisa"
                  select
                  label={
                    fieldError["validVisa"] ? "*Required Field" : "Valid Visa"
                  }
                  value={validVisa}
                  onChange={handleChangeValidVisa}
                  style={{ width: "100%" }}
                >
                  {visaList.map((option, index) => (
                    <MenuItem key={index} value={option}>
                      {option}
                    </MenuItem>
                  ))}
                </TextField>
              </Paper>
            </Grid>
            <Grid item xs={4}>
              <Paper className={classes.paper}>
                <TextField
                  error={fieldError["qualification"]}
                  id="standard-select-qualification"
                  select
                  label={
                    fieldError["qualification"]
                      ? "*Required Field"
                      : "Highst Qualification"
                  }
                  value={qualification}
                  onChange={handleChangeQualification}
                  style={{ width: "100%" }}
                >
                  {qualificationList.map((option, index) => (
                    <MenuItem key={index} value={option}>
                      {option}
                    </MenuItem>
                  ))}
                </TextField>
              </Paper>
            </Grid>

            <Grid item xs={1}></Grid>
            <Grid item xs={5}>
              <Paper className={classes.paper}>
                Consent of Storing Data:
                <Checkbox
                  value={allowStoring}
                  onChange={handleChangeAllowStoring}
                  color="primary"
                  inputProps={{ "aria-label": "secondary checkbox" }}
                />
              </Paper>
            </Grid>
            <Grid item xs={5}>
              <Paper className={classes.paper}>
                Subcription to mail Alerts from IBS :
                <Checkbox
                  value={allowSubscription}
                  onChange={handleChangeAllowSubcription}
                  color="primary"
                  inputProps={{ "aria-label": "secondary checkbox" }}
                />
              </Paper>
            </Grid>
          </Grid>
          <br />
        </div>
      </DialogContent>
      <DialogActions>
        <Grid
          container
          direction="row"
          justify="center"
          alignItems="center"
          spacing={2}
        >
          <Grid item xs={1}>
            <Button
              variant="contained"
              color="primary"
              method="post"
              onClick={onClickSubmit}
            >
              Submit
            </Button>
          </Grid>

          <Grid item xs={1}>
            <Button
              onClick={() => props.handleClose(false)}
              color="secondary"
              variant="contained"
            >
              Close
            </Button>
          </Grid>
        </Grid>
      </DialogActions>
    </Dialog>
  );
};

export default ResumeData;
