import React, { useState, useEffect } from "react";
import {
  makeStyles,
  Grid,
  Card,
  CardContent,
  Typography,
  CardActions,
  Paper,
  CardActionArea,
  Button,
} from "@material-ui/core";
import Axios from "axios";
import { Link } from "react-router-dom";

const useStyles = makeStyles((theme) => ({
  root1: {
    flexGrow: 1,
  },

  root: {
    // flexGrow: 1,
    padding: theme.spacing(2),
    minWidth: 100,
  },
  bullet: {
    display: "inline-block",
    margin: "0 2px",
    transform: "scale(0.8)",
  },
  paper: {
    padding: theme.spacing(2),
    textAlign: "center",
    color: theme.palette.text.secondary,
    height: "80px",
  },
  title: {
    fontSize: 14,
  },
  pos: {
    marginBottom: 12,
    textAlign: "center",
  },
}));

const JobOpeinings = (props) => {
  const classes = useStyles();
  const [jobOpenings, setJobOpenings] = useState([]);
  const { searchText, jobLocation } = props.searchValues;

  useEffect(() => {
    Axios.get(`http://localhost:8080/getAllJobOpenings`).then(
      (res) => {
        filterJobOpenings(res.data);
      },
      (err) => {
        console.log(err);
      }
    );
  }, [searchText, jobLocation]);

  const filterJobOpenings = (datas) => {
    datas = datas.filter((data) => {
      return data.status === "Active";
    });
    if (jobLocation !== "") {
      datas = datas.filter((data) => {
        if (data.jobLocation.includes(jobLocation)) {
          return true;
        }
        return false;
      });
    }
    if (searchText !== "") {
      datas = datas.filter((data) => {
        if (data.jobTitle.toLowerCase().includes(searchText.toLowerCase())) {
          return true;
        }
        return false;
      });
    }
    setJobOpenings(datas);
  };

  const getSearchResultHead = () => {
    let str = "";
    str = searchText === "" ? str : str + `Search Result On : ${searchText};`;
    str = jobLocation === "" ? str : str + `  Location : ${jobLocation}`;
    return <h3>{str}</h3>;
  };

  return (
    <div>
      <div>
        <center>{getSearchResultHead()}</center>
      </div>

      <div className={classes.root}>
        <Grid
          container
          direction="row"
          justify="center"
          alignItems="center"
          spacing={2}
        >
          {jobOpenings.length === 0 ? (
            <Grid item xs={4}>
              <Paper className={classes.paper}>
                <h2>No Openings Found!</h2>
              </Paper>
            </Grid>
          ) : (
            jobOpenings.map((jd) => {
              return (
                <Grid item xs={4}>
                  <Card className={classes.root}>
                    <CardActionArea>
                      <CardContent>
                        <Typography
                          gutterBottom
                          variant="h5"
                          component="h2"
                          style={{ color: "#00a4aa" }}
                        >
                          {jd.jobTitle}
                        </Typography>
                        <Typography variant="body2" color="textSecondary">
                          Job Location(s) : {jd.jobLocation}
                        </Typography>
                      </CardContent>
                    </CardActionArea>
                    <CardActions>
                      <Button
                        variant="contained"
                        component={Link}
                        to="/readmore/"
                        onClick={() => props.handleChangeReadMore(jd)}
                        style={{ backgroundColor: "#1c496a", color: "#fff" }}
                      >
                        Read More
                      </Button>
                    </CardActions>
                  </Card>
                </Grid>
              );
            })
          )}
        </Grid>
      </div>
    </div>
  );
};

export default JobOpeinings;
