import React, { useState } from "react";
import { makeStyles } from "@material-ui/core/styles";
import SearchContent from "./SearchContent";
import Copyright from "../Common/Copyright";
import { Box, Grid, Card, CardContent, Divider } from "@material-ui/core";
import IbsLogo from "../Common/IbsLogo";
import { Route, BrowserRouter, Link, Switch } from "react-router-dom";
import ReadMore from "./ReadMore";

import JobOpeinings from "./JobOpenings";


const useStyles = makeStyles((theme) => ({
  root: {
    flexGrow: 1,
    minWidth: 60,
    minHeight: 10,
  },
  table: {
    minWidth: 300,
  },
  toolbarIcon: {
    display: "flex",
    alignItems: "center",
    justifySource: "flex-end",
    padding: "0 8px",
    ...theme.mixins.toolbar,
  },
  root1: {
    display: "flex",
    "& > *": {
      margin: theme.spacing(1),
    },
  },
}));

export default function External() {
  const classes = useStyles();
  const [searchValues, setSearchValues] = useState({
    searchText: "",
    jobLocation: "",
  });

  const [readMoreJob, setReadMoreJob] = useState("");

  const headPicture = () => {
    return (
      <div className={classes.root}>
        <Grid container spacing={2}>
          <Grid item xs={12}>
            <Card className={classes.root}>
              <IbsLogo width={"40%"} />
              <center>
                <img
                  src={require("../assets/images/join.jpg")}
                  width="1350"
                  height="300"
                />
                <nr />
              </center>
              <CardContent></CardContent>
            </Card>
          </Grid>
        </Grid>
      </div>
    );
  };

  const handleSearchValues = (searchText, jobLocation) => {
    setSearchValues({ searchText: searchText, jobLocation: jobLocation });
  };

  const handleChangeReadMore = (job) => {
    setReadMoreJob(job);
  };

  return (
    <div className={classes.root}>
      {headPicture()}
      <br />
      <BrowserRouter>
        <Switch>
          <Route exact path="/">
            <>
              <SearchContent handleSearchValues={handleSearchValues} />
              <br />
              <Divider />
              <Divider />

              <JobOpeinings
                searchValues={searchValues}
                handleChangeReadMore={handleChangeReadMore}
              />
            </>
          </Route>
          <Route exact path="/readmore">
            <ReadMore selectedJob={readMoreJob} />
          </Route>
        </Switch>
      </BrowserRouter>
      <Box pt={4}>
        <Copyright />
      </Box>
    </div>
  );
}
