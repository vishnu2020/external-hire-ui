import React, { useState, useEffect } from "react";
import { makeStyles } from "@material-ui/core/styles";
import {
  Button,
  TextField,
  Grid,
  InputAdornment,
  Paper,
} from "@material-ui/core";

import Autocomplete from "@material-ui/lab/Autocomplete";
import { SERVER_URL } from "../Common/StaticContent";
import Axios from "axios";
import SearchIcon from "@material-ui/icons/Search";
import HistoryIcon from "@material-ui/icons/History";
import ApplicationHistory from "./ApplicationHistory";

const useStyles = makeStyles((theme) => ({
  formControl: {
    margin: theme.spacing(1),
    minWidth: 120,
    maxWidth: 300,
    textAlign: "center",
  },
  root: {
    flexGrow: 1,
    textAlign: "center",
  },
  paper: {
    padding: theme.spacing(2),
    textAlign: "center",
    // color: theme.palette.text.secondary,
  },
  button: {
    "& > *": {
      margin: theme.spacing(1),
    },
  },
  chips: {
    display: "flex",
    flexWrap: "wrap",
  },
  chip: {
    margin: 2,
  },
  noLabel: {
    marginTop: theme.spacing(3),
  },
}));

const ITEM_HEIGHT = 48;
const ITEM_PADDING_TOP = 8;
const MenuProps = {
  PaperProps: {
    style: {
      maxHeight: ITEM_HEIGHT * 4.5 + ITEM_PADDING_TOP,
      width: 250,
    },
  },
};

function getStyles(name, personName, theme) {
  return {
    fontWeight:
      personName.indexOf(name) === -1
        ? theme.typography.fontWeightRegular
        : theme.typography.fontWeightMedium,
  };
}

const SearchContent = (props) => {
  const classes = useStyles();

  const [jobLocation, setJobLocation] = useState("");
  const [searchText, setSearchText] = useState("");
  const [jobLocationList, setJobLocationList] = useState([]);
  const [openHistory, setOpenHistoty] = useState(false);

  const handleCloseHistory = () => {
    setOpenHistoty(false);
  };

  const handleChangeSearchText = (event) => {
    setSearchText(event.target.value);
  };

  const handleChangejobLocation = (event, selected) => {
    selected === null ? setJobLocation("") : setJobLocation(selected);
  };

  const handleClickFindJob = () => {
    // alert(searchText + "," + jobLocation);
    props.handleSearchValues(searchText, jobLocation);
  };

  useEffect(() => {
    Axios.get(`${SERVER_URL}/getLocation`).then(
      (res) => {
        setJobLocationList(res.data.map((loc) => loc.location));
      },
      (err) => {
        console.log(err);
      }
    );
  }, []);

  return (
    <div className={classes.root}>
      <Grid
        container
        direction="row"
        justify="center"
        alignItems="center"
        spacing={3}
      >
        <Grid item xs={2}></Grid>
        <Grid item xs={3}>
          <Paper style={{ height: "60px" }}>
            <TextField
              id="outlined-basic"
              placeholder="Search Keyword"
              size="small"
              value={searchText}
              onChange={handleChangeSearchText}
              style={{ width: "90%", height: "120%", paddingTop: "19px" }}
              InputProps={{
                startAdornment: (
                  <InputAdornment position="start">
                    <SearchIcon />
                  </InputAdornment>
                ),
              }}
            />
          </Paper>
        </Grid>
        <Grid item xs={3}>
          <Paper style={{ height: "60px" }}>
            <Autocomplete
              {...{ options: jobLocationList }}
              id="standard-select-joblocation"
              value={jobLocation}
              onChange={handleChangejobLocation}
              style={{ width: "90%", paddingLeft: "15px" }}
              renderInput={(params) => (
                <TextField {...params} label="Location" />
              )}
            />
          </Paper>
        </Grid>
        <Grid item xs={2}>
          <Button
            variant="contained"
            color="primary"
            onClick={handleClickFindJob}
          >
            Find Job
          </Button>
        </Grid>
        <Grid item xs={1}>
          <Paper style={{ height: "40px", width: "40px" }}>
            <HistoryIcon
              color="primary"
              fontSize="large"
              style={{ paddingTop: "4px" }}
              onClick={() => setOpenHistoty(true)}
            />
          </Paper>
        </Grid>
        <Grid item xs={1}></Grid>
      </Grid>
      {openHistory ? (
        <ApplicationHistory
          open={openHistory}
          handleClose={handleCloseHistory}
        />
      ) : null}
    </div>
  );
};

export default SearchContent;
