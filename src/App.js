import React from "react";
import { BrowserRouter as Router, Switch, Route } from "react-router-dom";
import External from "./external/External";
import Layout from "./Layout/Layout";

function App() {
  localStorage.setItem("isAdmin", false);
  return (
    <Router>
      <Switch>
        <Route exact path="">
          <div className="App">
            <Layout /> 
            {/* <External /> */}
          </div>
        </Route>
      </Switch>
    </Router>
  );
}

export default App;
