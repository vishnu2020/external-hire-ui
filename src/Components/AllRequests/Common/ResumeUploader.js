import { Button, CircularProgress } from '@material-ui/core'
import { CloudUploadOutlined } from '@material-ui/icons'
import Axios from 'axios'
import React from 'react'

export default function ResumeUploader(props) {
    const [fileUploadProgress, setFileUploadProgress] = React.useState(false)

    const fileChange = (e) => {
        if(e.target.files.length === 0)
            return;
        setFileUploadProgress(true);
        console.log(e.target.files)
        let form = new FormData();
        for (const file of e.target.files) {
            form.append("resumes[]", file, file.name)
          }
        const config = {     
            headers: { 'content-type': 'multipart/form-data' }
        }
        Axios.post("http://localhost:8080/uploadResumes", form, config).then(res => {
            console.log(res.data);
            setFileUploadProgress(false);
            props.fetchNewCandidate(res.data);
        }).catch(err => console.log(err))
    }
    return (
        <>
            <Button
                variant="text"
                component="label"
                disabled={fileUploadProgress}
                >
                {fileUploadProgress ? <CircularProgress size={25}/> : <CloudUploadOutlined/>}
                <div style={{paddingLeft:"5px"}}>Resume</div>
                <input
                    type="file"
                    multiple
                    style={{ display: "none" }}
                    onChange={fileChange}
                />
            </Button>
        </>
    )
}
