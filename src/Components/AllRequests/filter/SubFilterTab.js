import React from 'react';
import PropTypes from 'prop-types';
import { makeStyles } from '@material-ui/core/styles';
import Tabs from '@material-ui/core/Tabs';
import Tab from '@material-ui/core/Tab';
import Typography from '@material-ui/core/Typography';
import Box from '@material-ui/core/Box';
import { List, ListItem, ListItemIcon, Checkbox, ListItemText, Button, Grid } from '@material-ui/core';

function TabPanel(props) {
  const { children, value, index, ...other } = props;

  return (
    <div
      role="tabpanel"
      hidden={value !== index}
      id={`vertical-tabpanel-${index}`}
      aria-labelledby={`vertical-tab-${index}`}
      {...other}
    >
      {value === index && (
        <Box p={1}>
          <Typography>{children}</Typography>
        </Box>
      )}
    </div>
  );
}

TabPanel.propTypes = {
  children: PropTypes.node,
  index: PropTypes.any.isRequired,
  value: PropTypes.any.isRequired,
};

function a11yProps(index) {
  return {
    id: `vertical-tab-${index}`,
    'aria-controls': `vertical-tabpanel-${index}`,
  };
}

const useStyles = makeStyles((theme) => ({
  root: {
    flexGrow: 1,
    backgroundColor: theme.palette.background.paper,
    display: 'flex',
    height: 700,
  },
  tabs: {
    borderRight: `1px solid ${theme.palette.divider}`,
    borderBottom: `1px solid ${theme.palette.divider}`,
    width:"40%"
  },
  tabpanel: {
    overflowY:"scroll", 
    width:"60%", 
    height:"100%",
    borderBottom: `1px solid ${theme.palette.divider}`,
  },
  button: {
    marginLeft:"10px"
  }
}));

export default function SubFilterTab(props) {
  const classes = useStyles();
  const [value, setValue] = React.useState(0);
  const [expChecked, setExpChecked] = React.useState([]);
  const [qualificiationChecked, setQualificiationChecked] = React.useState([]);
  const [skillsChecked, setCheckedSkills] = React.useState([]);

  const filterArray = (array) => {
    return array.filter(function(item, pos){
      return array.indexOf(item)== pos; 
    });
  }

  const experienceFilterContent = () => {
    var experienceValues = props.data.map(exp => exp.experience);
    return filterArray(experienceValues);
    // return experienceValues;
  }

  const skillFilterContent = () => {
    var skillValues = props.data.map(exp => exp.skills);
    return filterArray(skillValues);
  }

  const qualificationFilterContent = () => {
    var qualificationValues = props.data.map(exp => exp.qualification);
    return filterArray(qualificationValues);
  }

  const setSelectedExp = (value) => () => {
    const currentIndex = expChecked.indexOf(value);
    const newChecked = [...expChecked];

    if (currentIndex === -1) {
      newChecked.push(value);
    } else {
      newChecked.splice(currentIndex, 1);
    }
    setExpChecked(newChecked);
  };

  const setSelectedQualification = (value) => () => {
    const currentIndex = qualificiationChecked.indexOf(value);
    const newChecked = [...qualificiationChecked];

    if (currentIndex === -1) {
      newChecked.push(value);
    } else {
      newChecked.splice(currentIndex, 1);
    }
    setQualificiationChecked(newChecked);
  };

  const setSelectedSkills = (value) => () => {
    const currentIndex = skillsChecked.indexOf(value);
    const newChecked = [...skillsChecked];

    if (currentIndex === -1) {
      newChecked.push(value);
    } else {
      newChecked.splice(currentIndex, 1);
    }
    setCheckedSkills(newChecked);
  };

  const handleChange = (event, newValue) => {
    setValue(newValue);
  };

  const setFilter = () => {
    const filters = { experience: expChecked, qualification: qualificiationChecked, skills: skillsChecked }
    props.setFilter(filters)
  }

  const clearFilter = () => {
    setExpChecked([]);
    setQualificiationChecked([]);
    setCheckedSkills([]);
    setFilter();
  }

  return (
    <Grid container>
      <Grid className={classes.root} item xs={12}>
      <Tabs
        orientation="vertical"
        variant="scrollable"
        value={value}
        onChange={handleChange}
        aria-label="Vertical tabs example"
        className={classes.tabs}
      >
        <Tab label="Experience" {...a11yProps(0)} />
        <Tab label="Skills" {...a11yProps(1)} />
        <Tab label="Qualification" {...a11yProps(2)} />
      </Tabs>
      <TabPanel className={classes.tabpanel} value={value} index={0}>
        <List>
        {experienceFilterContent().map((value) => {
            const labelId = `checkbox-list-label-${value}`;
            return (
            <ListItem key={value} role={undefined} dense onClick={setSelectedExp(value)}>
                <ListItemIcon>
                <Checkbox
                    edge="start"
                    checked={expChecked.indexOf(value) !== -1}
                    tabIndex={-1}
                    disableRipple
                    inputProps={{ 'aria-labelledby': labelId }}
                />
                </ListItemIcon>
                <ListItemText id={labelId} primary={value} />
            </ListItem>
            );
            })}
        </List>
      </TabPanel>
      <TabPanel className={classes.tabpanel} value={value} index={1}>
        <List>
          {skillFilterContent().map((value) => {
              const labelId = `checkbox-list-label-${value}`;
              return (
              <ListItem key={value} role={undefined} dense onClick={setSelectedSkills(value)}>
                  <ListItemIcon>
                  <Checkbox
                      edge="start"
                      checked={skillsChecked.indexOf(value) !== -1}
                      tabIndex={-1}
                      disableRipple
                      inputProps={{ 'aria-labelledby': labelId }}
                  />
                  </ListItemIcon>
                  <ListItemText id={labelId} primary={value} />
              </ListItem>
              );
          })}
        </List>
      </TabPanel>
      <TabPanel className={classes.tabpanel} value={value} index={2}>
        <List>
            {qualificationFilterContent().map((value) => {
                const labelId = `checkbox-list-label-${value}`;
                return (
                <ListItem key={value} role={undefined} dense onClick={setSelectedQualification(value)}>
                    <ListItemIcon>
                    <Checkbox
                        edge="start"
                        checked={qualificiationChecked.indexOf(value) !== -1}
                        tabIndex={-1}
                        disableRipple
                        inputProps={{ 'aria-labelledby': labelId }}
                    />
                    </ListItemIcon>
                    <ListItemText id={labelId} primary={value} />
                </ListItem>
                );
            })}
        </List>
      </TabPanel>
      </Grid> 
      <Grid item xs={12}>
        <div style={{position:"fixed",right:0,bottom:0, margin:"10px"}}>
          <Button className={classes.button} onClick={clearFilter} variant="contained" color="inherit">Clear</Button>
          <Button className={classes.button} onClick={setFilter} variant="contained" color="inherit">Filter</Button>
        </div>
      </Grid>
    </Grid>
  );
}
