import React from 'react';
import clsx from 'clsx';
import { makeStyles } from '@material-ui/core/styles';
import Drawer from '@material-ui/core/Drawer';
import Button from '@material-ui/core/Button';
import List from '@material-ui/core/List';
import Divider from '@material-ui/core/Divider';
import ListItem from '@material-ui/core/ListItem';
import ListItemIcon from '@material-ui/core/ListItemIcon';
import ListItemText from '@material-ui/core/ListItemText';
import InboxIcon from '@material-ui/icons/MoveToInbox';
import MailIcon from '@material-ui/icons/Mail';
import { Typography, Checkbox, IconButton, ListSubheader } from '@material-ui/core';
import SubFilterTab from './SubFilterTab';

const useStyles = makeStyles({
  list: {
    width: 450,
  },
  fullList: {
    width: 'auto',
  },
});

export default function FilterCandidates(props) {
  const classes = useStyles();
  const [show, setState] = React.useState(props.show);

  const toggleDrawer = (open) => (event) => {
    setState(false);
    props.toggleFilter()
  };

  const setFilter = (filters) => {
    props.setFilter(filters);
  }

  const list = () => (
    <div
      className={clsx(classes.list)}
      role="presentation"
    //   onClick={toggleDrawer(false)}
    //   onKeyDown={toggleDrawer(false)}
    >
      <SubFilterTab setFilter={setFilter} data={props.data}/>
    </div>
  );

  return (
    // <div>
    //   {['right'].map((anchor) => (
        <React.Fragment>
          {/* <Button onClick={toggleDrawer(anchor, true)}>{anchor}</Button> */}
          <Drawer style={{display:"block"}} anchor="right" open={props.show} onClose={toggleDrawer(false)}>
            {list()}
          </Drawer>
        </React.Fragment>
    //   ))}
    // </div>
  );
}
