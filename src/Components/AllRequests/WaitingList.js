import React, { Component } from 'react';
import { Grid, Paper, Typography, Button, Link } from '@material-ui/core';
import { AgGridReact } from 'ag-grid-react';

import 'ag-grid-community/dist/styles/ag-grid.css';
import 'ag-grid-community/dist/styles/ag-theme-alpine.css';
import Axios from 'axios';
import ArrowForwardIosIcon from '@material-ui/icons/ArrowForwardIos';
import FilterCandidates from './filter/FilterCandidates';
import Alert from '@material-ui/lab/Alert';
import ResumeUploader from './Common/ResumeUploader';
var qs = require('qs');

export default class WaitingList extends Component {

    constructor(props) {
        super(props);
        this.state = {
            open: false,
            columnDefs: [
                { headerName: "Id", field: "candidateId", sortable: true, filter: true, width:80},
                { headerName: "Name", valueGetter: function(params) {
                    return params.data.firstName + ' ' + params.data.lastName + ' ' + params.data.middleName;
                  }, sortable: true, filter: true, width: 300 },
                { headerName: "Status", field: "status", sortable: true, filter: true, width:150},
                { headerName: "Experience", field: "experience", sortable: true, filter: true, width:160},
                { headerName: "Skills", field: "skills", filter: true, resizable: true },
                { headerName: "Qualification", field: "qualification", sortable: true, filter: true },
                { headerName: "EmailId", field: "emailId", sortable: true, filter: true },
                { headerName: "Mobile", field: "phoneNumber" , filter: true}
                ],
            rowData: [],
            gridApi: null,
            show:false,
            filters:{ experience: [], qualification: [], skills: [] }
        }
        localStorage.setItem("filter", JSON.stringify({ experience: [], qualification: [], skills: [] }))
        this.getCandidateDetailsFromUploadedResume = this.getCandidateDetailsFromUploadedResume.bind(this);
    }

    componentDidMount() {
        this.fetchAllCandidates()
    }

    showAllFilters() {
        const { experience, qualification, skills } = JSON.parse(localStorage.getItem("filter"));
        return [...experience, ...qualification, ...skills];
    }

    fetchAllCandidates(filters) {

        Axios.get("http://localhost:8080/getAllCandidate/waitinglist", 
        {
            params: {
                experience: filters ? filters.experience : [],
                qualification: filters ? filters.qualification : [], 
                skills: filters ? filters.skills : []
            },
            paramsSerializer: function(params) {
               return qs.stringify(params, {arrayFormat: 'repeat'})
            },
        }
          )
        .then(response => this.setState({rowData: response.data, show: false}))
        .then(err => console.log(err));
    }

    toggleFilter = () => {
        this.setState({show: !this.state.show})
    }

    setFilter = (filters) => {
        localStorage.setItem("filter",JSON.stringify(filters));
        this.fetchAllCandidates(filters)
    }

    getCandidateDetailsFromUploadedResume = (candidateList) => {
        this.setState({rowData: [...this.state.rowData, ...candidateList]});
    }

    render() {
        return (
            <>
                <Grid container spacing={1}>
                    <Grid style={{textAlign:"right"}} item xs={12}>
                        <ResumeUploader fetchNewCandidate={this.getCandidateDetailsFromUploadedResume}/>
                    </Grid>
                    <Grid container item xs={10}>
                        {
                            this.showAllFilters().map(filter => (<Alert style={{clear:"both",float:"left", margin:"3px"}} icon={false}>{filter}</Alert>))
                        }
                        {!this.showAllFilters() ? "clearAll" : ""}
                    </Grid>
                    <Grid style={{textAlign:"right"}} item xs={2}>
                        <Link
                            component="button"
                            variant="body2"
                            onClick={this.toggleFilter}
                            >
                            Advanced Search <ArrowForwardIosIcon fontSize="inherit"/>
                        </Link>
                    </Grid>
                    <Grid item xs={12}>
                        <FilterCandidates setFilter={this.setFilter} data={this.state.rowData} toggleFilter={this.toggleFilter} show={this.state.show}/>
                    </Grid>
                    <Grid item xs={12}>
                        <div className="ag-theme-alpine" style={ {height: '600px', width: '100%'} }>
                            <AgGridReact
                                columnDefs={this.state.columnDefs}
                                rowData={this.state.rowData}
                                onGridReady={ params => this.gridApi = params.api }
                                >
                            </AgGridReact>
                        </div>
                    </Grid>
                </Grid>
            </>
        )
    }
}

