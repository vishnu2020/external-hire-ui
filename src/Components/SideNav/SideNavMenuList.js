import React from "react";
import ListItem from "@material-ui/core/ListItem";
import ListItemIcon from "@material-ui/core/ListItemIcon";
import ListItemText from "@material-ui/core/ListItemText";
import SearchIcon from "@material-ui/icons/Search";
import PauseCircleOutlineIcon from "@material-ui/icons/PauseCircleOutline";
import CheckIcon from "@material-ui/icons/Check";
import DoneAllIcon from "@material-ui/icons/DoneAll";
import ClearIcon from "@material-ui/icons/Clear";
import "./SideNavMenuList.css";
import { List, Collapse, makeStyles, Divider } from "@material-ui/core";
import FilterListIcon from "@material-ui/icons/FilterList";
import KeyboardArrowDownIcon from "@material-ui/icons/KeyboardArrowDown";
import KeyboardArrowRightIcon from "@material-ui/icons/KeyboardArrowRight";
import AddIcon from "@material-ui/icons/Add";
import LinkIcon from "@material-ui/icons/Link";
import ViewCarouselIcon from "@material-ui/icons/ViewCarousel";
import {EmojiPeople} from '@material-ui/icons';
import { Link } from 'react-router-dom';
import HistoryIcon from '@material-ui/icons/History';
import GroupAddIcon from '@material-ui/icons/GroupAdd';
import AssignmentIcon from '@material-ui/icons/Assignment';
import ReportIcon from "@material-ui/icons/Report";

const useStyles = makeStyles((theme) => ({
  nested: {
    paddingLeft: theme.spacing(4),
    paddingRight: theme.spacing(2),
  },
  overrideLinkStyle: {
    textDecoration: "inherit",
    color: "inherit",
    cursor: "auto",
  },
}));

const SideNavMenuList = (props) => {
  const classes = useStyles();
  const commonSubItems = [
    {
      name: "Waitlisted",
      icon: <PauseCircleOutlineIcon />,
      link: "/waitingList", 
    },
    {
      name: "Shortlisted",
      icon: <CheckIcon />,
      link: "/shortList",
    },
    {
      name: "Selected",
      icon: <DoneAllIcon />,
      link: "/selected",
    },
    {
      name: "Rejected",
      icon: <ClearIcon />,
      link: "/rejected",
    },
    // {
    //   name: "By Skill Set",
    //   icon: <SearchIcon />,
    //   link: "", //"/seachSkills"
    // },
    // {
    //   name: "By Created Date",
    //   icon: <FilterListIcon />,
    //   link: "", //"/searchCreatedDate"
    // },
  ];

  const itemNames = [
    {
      name: "All Request",
      subItems: commonSubItems,
      mainLink: "/request",
    },
    // {
    //   name: "Career Portal",
    //   subItems: commonSubItems,
    //   mainLink: "/request-careerPortal",
    // },
    // {
    //   name: "Referral Portal",
    //   subItems: commonSubItems,
    //   mainLink: "/request-referalPortal",
    // },
    {
      name: "Job Openings",
      mainLink: "/jobOpenings",
      subItems: [
        {
          name: "Create Opening",
          icon: <AddIcon />,
          link: "/CreateOpening",
        },
        {
          name: "View Openings",
          icon: <ViewCarouselIcon />,
          link: "/viewOpenings",
        },
        {
          name: "Job Allocation",
          icon: <LinkIcon />,
          link: "/jobAllocation",
        },
      ],
    },
    {
      name: "Demands",
      mainLink: "/demand",
      subItems: [
        {
          name: "Open Demands",
          icon: <EmojiPeople />,
          link: "/openDemand",
        },
        {
          name: "Assigned Status",
          icon: <AssignmentIcon/>,
          link: "/history"
        },
        {
          name: "Candidate mapping",
          icon: <GroupAddIcon/>,
          link: "/candidateMapping"
        }
      ]
    },
     {
       name: "Reports",
       mainLink: "/reports",
       subItems: [
         {
           name: "Report 1",
           icon: <ReportIcon />,
           link: "/report1",
         },
         {
           name: "Report 2",
           icon: <ReportIcon />,
           link: "/report2",
         },
       ],
     }
  ];

  const initOpen = () => {
    let totalList = [];
    for (let i = 0; i < itemNames.length; i++) {
      totalList.push(false);
    }
    return totalList;
  };

  const [open, setOpen] = React.useState(initOpen);

  const handleClick = (index) => {
    setOpen(
      open.slice().map((lo, idx) => {
        if (idx === index) {
          return !lo;
        }
        return false;
      })
    );
  };

  return (
    <List>
      {itemNames.map((itemName, index) => {
        return (
          <div key={index}>
            <ListItem
              button
              selected={itemName.name === props.selected.source}
              onClick={() => {
                handleClick(index);
              }}
              style={{ background: "#8b8b8c" }}
            >
              <ListItemIcon style={{ color: "#fff" }}>
                {open[index] ? (
                  <KeyboardArrowRightIcon />
                ) : (
                  <KeyboardArrowDownIcon />
                )}
              </ListItemIcon>
              <ListItemText primary={itemName.name} style={{ color: "#fff" }} />
            </ListItem>
            <Divider light />
            <Collapse in={open[index]} timeout="auto" unmountOnExit>
              <List component="div" disablePadding>
                {itemName.subItems.map((subItem, idx) => {
                  return (
                    <Link
                      key={idx}
                      className={classes.overrideLinkStyle}
                      to={itemName.mainLink + subItem.link}
                    >
                      <ListItem
                        selected={subItem.name === props.selected}
                        button
                        className={classes.nested}
                        onClick={() => {
                          props.setSource(subItem.name);
                        }}
                      >
                        <ListItemIcon>{subItem.icon}</ListItemIcon>
                        <ListItemText primary={subItem.name} />
                      </ListItem>
                    </Link>
                  );
                })}
              </List>
            </Collapse>
          </div>
        );
      })}
    </List>
  );
};

export default SideNavMenuList;
