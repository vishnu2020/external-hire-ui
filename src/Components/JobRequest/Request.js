import React, { useState, useEffect } from "react";
import clsx from "clsx";
import { makeStyles } from "@material-ui/core/styles";
import Table from "@material-ui/core/Table";
import TableBody from "@material-ui/core/TableBody";
import TableCell from "@material-ui/core/TableCell";
import TableContainer from "@material-ui/core/TableContainer";
import TableHead from "@material-ui/core/TableHead";
import TablePagination from "@material-ui/core/TablePagination";
import TableRow from "@material-ui/core/TableRow";
import TableSortLabel from "@material-ui/core/TableSortLabel";
import Toolbar from "@material-ui/core/Toolbar";
import Typography from "@material-ui/core/Typography";
import Paper from "@material-ui/core/Paper";
import GetAppIcon from "@material-ui/icons/GetApp";

const createData = (
  id,
  name,
  jobpos,
  experience,
  contactNo,
  email,
  hrAction,
  createdOn,
  respondedOn,
  source
) => {
  return {
    id,
    name,
    jobpos,
    experience,
    contactNo,
    email,
    hrAction,
    createdOn,
    respondedOn,
    source,
  };
};

const rows = [
  createData(
    1,
    "Cupcake",
    "Python Dev",
    3.7,
    12345678,
    "ccdjc@cjd.com",
    "Selected",
    "15-02-2019",
    "25-03-2019",
    "Referral",
    "oracle"
  ),
  createData(
    2,
    "Cupcake",
    "SQL Server",
    3.7,
    12345678,
    "ccdjc@cjd.com",
    "Rejected",
    "15-02-2019",
    "25-03-2019",
    "Referral"
  ),
  createData(
    3,
    "Cupcake",
    "Database Designer",
    3.7,
    12345678,
    "ccdjc@cjd.com",
    "Waitlisted",
    "15-02-2019",
    "25-03-2019",
    "Referral"
  ),
  createData(
    4,
    "Cupcake",
    "Java Tesing",
    3.7,
    12345678,
    "ccdjc@cjd.com",
    "Selected",
    "15-02-2019",
    "25-03-2019",
    "Referral"
  ),
  createData(
    5,
    "Cupcake",
    "Software Engg.",
    3.7,
    12345678,
    "ccdjc@cjd.com",
    "Rejected",
    "15-02-2019",
    "25-03-2019",
    "Career"
  ),
  createData(
    6,
    "Cupcake",
    "Automation Tesing",
    2,
    12345678,
    "ccdjc@cjd.com",
    "Selected",
    "15-02-2019",
    "25-03-2019",
    "Career"
  ),
  createData(
    7,
    "A-4278",
    "Cupcake",
    "Python Dev",
    3.2,
    12345678,
    "ccdjc@cjd.com",
    "Shortlisted",
    "15-02-2019",
    "25-03-2019",
    "Referral"
  ),
  createData(
    8,
    "Cupcake",
    "Java Dev",
    3.7,
    12345678,
    "ccdjc@cjd.com",
    "Selected",
    "15-02-2019",
    "25-03-2019",
    "Career"
  ),
  createData(
    9,
    "Cupcake",
    "SQL server",
    3.7,
    12345678,
    "cddf@s.ck",
    "Waitlisted",
    "15-02-2019",
    "25-03-2019",
    "Referral"
  ),
  createData(
    10,
    "Cupcake",
    "Oracle",
    3,
    12345678,
    "skdjksl@dkl.co",
    "Selected",
    "15-02-2019",
    "25-03-2019",
    "Career"
  ),
  createData(
    11,
    "Cupcake",
    "Java testing",
    3.9,
    12345678,
    "cjdshck@dfnkdlf.vom",
    "Rejected",
    "15-02-2019",
    "25-03-2019",
    "Career"
  ),
  createData(
    12,
    "Cupcake",
    "Jave Dev",
    4.7,
    12345678,
    "abcd@xue.com",
    "Selected",
    "15-02-2019",
    "25-03-2019",
    "Career"
  ),
];
// id, name, jobpos, experience, contactNo, email, hrAction, createdOn, respondedOn, source
const headCells = [
  { id: "id", label: "ID", width: "short" },
  { id: "name", label: "Name", width: "long" },
  { id: "jobpos", label: "Job Position", width: "long" },
  { id: "experience", label: "Experience", width: "short" },
  { id: "contactNo", label: "Contact No.", width: "long" },
  { id: "email", label: "Email ID", width: "long" },
  { id: "hrAction", label: "HR Action", width: "long" },
  { id: "createdOn", label: "Created On", width: "short" },
  { id: "respondedOn", label: "Responded On", width: "short" },
  { id: "source", label: "Source", width: "long" },
  { id: "downloadIcon", label: "", width: "short" },
];

const useToolbarStyles = makeStyles((theme) => ({
  root: {
    paddingLeft: theme.spacing(2),
    paddingRight: theme.spacing(1),
  },
  title: {
    flex: "1 1 100%",
  },
}));

const EnhancedTableToolbar = (props) => {
  const classes = useToolbarStyles();
  return (
    <Toolbar className={clsx(classes.root)}>
      <Typography
        className={classes.title}
        variant="h6"
        id="tableTitle"
        component="div"
      >
        {props.titleSource.source + " > " + props.titleSource.subSource}
      </Typography>
    </Toolbar>
  );
};

const EnhancedTableHead = (props) => {
  const { classes, order, orderBy, onRequestSort, titleSource } = props;
  const createSortHandler = (property) => (event) => {
    onRequestSort(event, property);
  };

  return (
    <TableHead>
      <TableRow tabIndex={-1}>
        {headCells.map((headCell) =>
          titleSource.source !== "All Request" &&
          headCell.id === "source" ? null : (
            <TableCell
              className={classes.tablehead}
              key={headCell.id}
              align="center"
              sortDirection={orderBy === headCell.id ? order : false}
              onClick={createSortHandler(headCell.id)}
            >
              {headCell.label}
              {headCell.label === "" ? null : (
                <TableSortLabel
                  active={orderBy === headCell.id}
                  direction={orderBy === headCell.id ? order : "asc"}
                >
                  {orderBy === headCell.id ? (
                    <span className={classes.visuallyHidden}>
                      {order === "desc"
                        ? "sorted descending"
                        : "sorted ascending"}
                    </span>
                  ) : null}
                </TableSortLabel>
              )}
            </TableCell>
          )
        )}
      </TableRow>
    </TableHead>
  );
};

const useStyles = makeStyles((theme) => ({
  root: {
    width: "100%",
  },
  paper: {
    width: "100%",
    marginBottom: theme.spacing(2),
  },
  table: {
    minWidth: 750,
  },
  tablehead: {
    background: "#85adad",
    color: "#fff",
    fontSize: "120%",
  },
  container: {
    maxHeight: 400,
  },
  visuallyHidden: {
    border: 0,
    clip: "rect(0 0 0 0)",
    height: 1,
    margin: -1,
    overflow: "hidden",
    padding: 0,
    position: "absolute",
    top: 20,
    width: 1,
  },
  value: {
    paddingRight: theme.spacing(6),
  },
}));

const Request = (props) => {
  const classes = useStyles();
  const [order, setOrder] = useState("asc");
  const [titleSource, setTitleSource] = useState(props.titleSource);
  const [orderBy, setOrderBy] = useState("calories");
  const [page, setPage] = useState(0);
  const [rowsPerPage, setRowsPerPage] = useState(5);

  useEffect(() => {
    setPage(0);
    setTitleSource(props.titleSource);
  }, [props.titleSource]);

  const handleRequestSort = (event, property) => {
    const isAsc = orderBy === property && order === "asc";
    setOrder(isAsc ? "desc" : "asc");
    setOrderBy(property);
  };

  const handleChangePage = (event, newPage) => {
    setPage(newPage);
  };

  const handleChangeRowsPerPage = (event) => {
    setRowsPerPage(parseInt(event.target.value, 10));
    setPage(0);
  };

  const descComparator = (a, b, orderBy) => {
    if (b[orderBy] < a[orderBy]) {
      return -1;
    }
    if (b[orderBy] > a[orderBy]) {
      return 1;
    }
    return 0;
  };

  const stableSort = (array) => {
    const stabilizedThis = array.map((el, index) => [el, index]);
    stabilizedThis.sort((a, b) => {
      const orderVal =
        order === "desc"
          ? descComparator(a[0], b[0], orderBy)
          : -descComparator(a[0], b[0], orderBy);
      if (orderVal !== 0) return orderVal;
      return a[1] - b[1];
    });
    return stabilizedThis.map((el) => el[0]);
  };

  const emptyRows =
    rowsPerPage - Math.min(rowsPerPage, rows.length - page * rowsPerPage);
  const rowsToDisplay = stableSort(rows).slice(
    page * rowsPerPage,
    page * rowsPerPage + rowsPerPage
  );

  return (
    <div className={classes.root}>
      <Paper className={classes.paper}>
        <EnhancedTableToolbar titleSource={titleSource} />
        <TableContainer className={classes.container}>
          <Table
            className={classes.table}
            stickyHeader
            aria-labelledby="tableTitle"
            size="medium"
            aria-label="enhanced table"
          >
            <EnhancedTableHead
              classes={classes}
              order={order}
              orderBy={orderBy}
              titleSource={titleSource}
              onRequestSort={handleRequestSort}
            />
            <TableBody>
              {rowsToDisplay.map((row, index) => {
                // id, empId, name, skills, experience, contactNo, hrAction
                return (
                  <TableRow hover role="checkbox" tabIndex={-1} key={row.id}>
                    {headCells.map((headCell, idx) => {
                      return titleSource.source !== "All Request" &&
                        headCell.id === "source" ? null : (
                        <TableCell
                          className={classes.value}
                          key={idx}
                          align="center"
                        >
                          {headCell.id === "downloadIcon" ? (
                            <GetAppIcon />
                          ) : (
                            row[headCell.id]
                          )}
                        </TableCell>
                      );
                    })}
                  </TableRow>
                );
              })}
              {emptyRows > 0 && (
                <TableRow style={{ height: 53 * emptyRows }}>
                  <TableCell colSpan={6} />
                </TableRow>
              )}
            </TableBody>
          </Table>
        </TableContainer>
        <TablePagination
          rowsPerPageOptions={[3, 5, 10, 25]}
          component="div"
          count={rows.length}
          rowsPerPage={rowsPerPage}
          page={page}
          onChangePage={handleChangePage}
          onChangeRowsPerPage={handleChangeRowsPerPage}
        />
      </Paper>
    </div>
  );
};

export default Request;
