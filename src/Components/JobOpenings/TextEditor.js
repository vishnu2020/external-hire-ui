import React, { Component } from "react";
import "react-draft-wysiwyg/dist/react-draft-wysiwyg.css";
import { EditorState, convertToRaw } from "draft-js";
import { Editor } from "react-draft-wysiwyg";
import draftToHtml from "draftjs-to-html";
import { Button } from "@material-ui/core";

const wrapperStyle = {
  background: "#fff",
  borderStyle: "ridge",
  width: "100%",
};

const wrapperErrorStyle = {
  background: "#fff",
  borderStyle: "ridge",
  width: "100%",
  borderColor: "red",
};

const editorStyle = {
  background: "#fff",
  borderStyle: "outset",
  height: "200px",
  paddingLeft: "7px",
};

const toolbarStyle = { background: "#c2d6d6", borderStyle: "outset" };

export default class TextEditor extends Component {
  constructor(props) {
    super(props);
    this.state = {
      editorState: EditorState.createEmpty(),
    };
  }

  onEditorStateChange = (editorState) => {
    let htmlText = draftToHtml(convertToRaw(editorState.getCurrentContent()));
    this.props.onChange(htmlText);
    this.setState({
      editorState,
    });
  };

  ResetButton = () => {
    return (
      <Button
        color="secondary"
        variant="contained"
        onClick={() => this.onEditorStateChange(EditorState.createEmpty())}
        style={{ height: "32px" }}
      >
        Clear
      </Button>
    );
  };

  render() {
    const { editorState } = this.state;
    if (this.props.reset) {
      this.onEditorStateChange(EditorState.createEmpty());
    }
    return (
      <Editor
        spellCheck={true}
        placeholder={"Type here.."}
        editorState={editorState}
        wrapperClassName="demo-wrapper"
        editorClassName="demo-editor"
        toolbarCustomButtons={[<this.ResetButton />]}
        wrapperStyle={this.props.error ? wrapperErrorStyle : wrapperStyle}
        editorStyle={editorStyle}
        toolbarStyle={toolbarStyle}
        onEditorStateChange={this.onEditorStateChange}
      />
    );
  }
}
