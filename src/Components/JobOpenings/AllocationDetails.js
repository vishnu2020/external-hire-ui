import React, { useState, useEffect } from "react";
import { makeStyles, useTheme } from "@material-ui/core/styles";
import Button from "@material-ui/core/Button";
import Dialog from "@material-ui/core/Dialog";
import DialogContent from "@material-ui/core/DialogContent";
import DialogTitle from "@material-ui/core/DialogTitle";
import Chip from '@material-ui/core/Chip';
import {
  Grid,
  Toolbar,
  Paper,
  Typography,
  TableContainer,
  Table,
  TableHead,
  TableRow,
  TableCell,
  TableBody,
  Select,
  MenuItem,
  InputLabel,
  FormControl,
} from "@material-ui/core";
import clsx from "clsx";
import Axios from "axios";
import { SERVER_URL } from "../../Common/StaticContent";
import AlertDialog from "../../Common/Alert";
import LoadingContent from "../../Common/LoadingContent";
import Input from '@material-ui/core/Input';

const useToolbarStyles = makeStyles((theme) => ({
  root: {
    paddingLeft: theme.spacing(2),
    paddingRight: theme.spacing(1),
  },
  title: {
    flex: "1 1 100%",
  },
  paper: {
    padding: theme.spacing(2),
    textAlign: "center",
    justifyItems: "center",
    color: theme.palette.text.secondary,
    height: "40px",
  },
}));

const ITEM_HEIGHT = 48;
const ITEM_PADDING_TOP = 8;

const MenuProps = {
  PaperProps: {
    style: {
      maxHeight: ITEM_HEIGHT * 4.5 + ITEM_PADDING_TOP,
      width: 250,
    },
  },
};

const names = [
  'Oliver Hansen',
  'Van Henry',
  'April Tucker',
  'Ralph Hubbard',
  'Omar Alexander',
  'Carlos Abbott',
  'Miriam Wagner',
  'Bradley Wilkerson',
  'Virginia Andrews',
  'Kelly Snyder',
];

const EnhancedTableToolbar = (props) => {
  const classes = useToolbarStyles();
  return (
    <Toolbar className={clsx(classes.root)}>
      <Typography
        className={classes.title}
        variant="h6"
        id="tableTitle"
        component="div"
      >
        <h3>
          {"Job Openings > Allocation Details > Allocated Job ID - ID : " +
            props.id}
        </h3>
      </Typography>
    </Toolbar>
  );
};

const useStyles = makeStyles((theme) => ({
  root: {
    flexGrow: 1,
  },
  formControl: {
    margin: theme.spacing(1),
    minWidth: 100,
    // maxHeight: 50,
  },
  paper: {
    textAlign: "center",
    height: "45px",
    background: "#85adad",
    color: "#fff",
    fontSize: "130%",
  },
  headpaper: {
    textAlign: "center",
    justifyItems: "center",
    height: "80px",
    background: "#f0f5f5",
    fontSize: "130%",
  },
  subpaper: {
    paddingLeft: theme.spacing(4),
    paddingRight: theme.spacing(4),
    paddingTop: theme.spacing(1),
    paddingBottom: theme.spacing(1),
    textAlign: "center",
    background: "#ccffff",
    color: theme.palette.text.secondary,
  },
  table: {
    minWidth: 750,
  },
  tablehead: {
    background: "#8b8b8c",
    color: "#fff",
    fontSize: "120%",
  },
  container: {
    maxHeight: 400,
  },

  value: {
    paddingRight: theme.spacing(6),
    background: "#fff",
  },
  chips: {
    display: 'flex',
    flexWrap: 'wrap',
  },
  chip: {
    margin: 2,
  },
}));

const AllocationDetails = (props) => {
  const { jobDetails } = props;
  // console.log("details")
  // console.log(jobDetails);
  const classes = useStyles();
  const [open, setOpen] = useState([false, false, false]);
  const [fullScreen, setFullScreen] = useState(true);
  const [alertContent, setAlertContent] = useState({ content: "", type: "" });
  const [loading, setLoading] = useState(false);
  const theme = useTheme();
  const [personName, setPersonName] = React.useState([]);

  const demandDetailsHead = [
    { id: "id", label: "Demand ID" },
    { id: "role", label: "Role ID" },
    { id: "skills", label: "Skills" },
    { id: "accountName", label: "Account Name" },
    { id: "experience", label: "Experience" },
    { id: "requestedOn", label: "Requested On" },
  ];

  const applicantDetailsHead = [
    { id: "candidateId", label: "Applicant ID" },
    { id: "firstName", label: "Applicant Name" },
    { id: "skills", label: "Skills" },
    { id: "experience", label: "Experience" },
    { id: "appliedOn", label: "Apply On" },
    { id: "chooseDemand", label: "Choose Demand" },
    { id: "update", label: "" },
  ];

  const taggedApplicationHead = [
    { id: "candidateId", label: "Applicant ID" },
    { id: "firstName", label: "Applicant Name" },
    { id: "demandId", label: "Demand ID" },
    { id: "skills", label: "Skills" },
    { id: "department", label: "Depertment" },
  ];

  const [demandDetailsList, setDemandDetailsList] = useState([]);

  const [applicantDetailsList, setApplicantDetailsList] = useState([]);

  const [mappedCandidateToDemand, mapCandidateToDemand] = useState({});

  // getDemandForm

  // const [taggedApplicationList, setTaggedApplicationList] = useState([]);

  // useEffect(() => {
  //   if (loading) {
  //     Axios.get(
  //       `${SERVER_URL}/getApplicationsByJobId/${jobDetails.jobId}`
  //     ).then(
  //       (res) => {
  //         setApplicantDetailsList(res.data);
  //         setDemandDetailsList([
  //           {
  //             demandId: "1",
  //             skills: "Java",
  //             department: "Expedia",
  //             experience: "2-5 years",
  //             createdOn: "15-08-2020",
  //           },
  //           {
  //             demandId: "2",
  //             skills: "Java",
  //             department: "Expedia",
  //             experience: "2-5 years",
  //             createdOn: "15-08-2020",
  //           },
  //           {
  //             demandId: "3",
  //             skills: "Java",
  //             department: "Expedia",
  //             experience: "2-5 years",
  //             createdOn: "15-08-2020",
  //           },
  //           {
  //             demandId: "4",
  //             skills: "Java",
  //             department: "Expedia",
  //             experience: "2-5 years",
  //             createdOn: "15-08-2020",
  //           },
  //         ]);
  //         setMapApplicantToDemand(
  //           res.data.map((app) => {
  //             return { candidateId: app.candidateId, demandId: "" };
  //           })
  //         );
  //         setLoading(false);
  //         console.log(res.data);
  //       },
  //       (err) => {
  //         console.log(err);
  //       }
  //     );
  //   }
    //  **** Fetch List[] of Demands for a Job Openings ****
    // Axios.post(`${SERVER_URL}/getDemandsByJobId`, jobDetails.jobId).then(
    //   (res) => {
    //     setDemandDetailsList(res.data);
    //   },
    //   (err) => {
    //     console.log(err);
    //   }
    // );
    // setTaggedApplicationList(
    //   mapApplicantToDemand.map((a2d) => {
    //     let appName = "",
    //       skills = "",
    //       dept = "";
    //     demandDetailsList.forEach((demand) => {
    //       if (demand.demandId === a2d.demandId) {
    //         skills = demand.skills;
    //         dept = demand.department;
    //       }
    //     });
    //     applicantDetailsList.forEach((applicant) => {
    //       if (applicant.candidateId === a2d.candidateId) {
    //         appName = applicant.firstName;
    //       }
    //     });
    //     return {
    //       candidateId: a2d.candidateId,
    //       firstName: appName,
    //       demandId: a2d.demandId === "" ? "Not Selected" : a2d.demandId,
    //       skills: skills === "" ? "Not Selected" : skills,
    //       department: dept === "" ? "Not Selected" : dept,
    //     };
    //   })
    // );
  // }, [loading]);

  useEffect(() => {}, [mappedCandidateToDemand]);

  const handleClickDisplay = (index) => {
    setOpen(
      open.map((openVal, idx) => {
        return idx === index ? !openVal : openVal;
      })
    );
  };

  const getDemandDetails = () => {
    return jobDetails.demandGroup.demands.length === 0 ? (
      <Paper className={classes.headpaper}>
        <h3>No Demand Found</h3>
      </Paper>
    ) : (
      <TableContainer className={classes.container}>
        <Table
          className={classes.table}
          stickyHeader
          aria-labelledby="tableTitle"
          size="medium"
          aria-label="enhanced table"
        >
          <TableHead>
            <TableRow tabIndex={-1}>
              {demandDetailsHead.map((headCell) => (
                <TableCell
                  className={classes.tablehead}
                  key={headCell.id}
                  align="center"
                >
                  {headCell.label}
                </TableCell>
              ))}
            </TableRow>
          </TableHead>
          <TableBody>
            {jobDetails.demandGroup.demands.map((row, index) => {
              return (
                <TableRow hover role="checkbox" tabIndex={-1} key={index}>
                  {demandDetailsHead.map((headCell, idx) => {
                    return (
                      <TableCell
                        className={classes.value}
                        key={idx}
                        align="center"
                      >
                        {row[headCell.id]}
                      </TableCell>
                    );
                  })}
                </TableRow>
              );
            })}
          </TableBody>
        </Table>
      </TableContainer>
    );
  };

  const getApplicantDetails = () => {
    return jobDetails.candidateModels.length === 0 ? (
      <Paper className={classes.headpaper}>No Application Found</Paper>
    ) : (
      <TableContainer className={classes.container}>
        <Table
          className={classes.table}
          stickyHeader
          aria-labelledby="tableTitle"
          size="medium"
          aria-label="enhanced table"
        >
          <TableHead>
            <TableRow tabIndex={-1}>
              {applicantDetailsHead.map((headCell) => (
                <TableCell
                  className={classes.tablehead}
                  key={headCell.id}
                  // align="center"
                >
                  {headCell.label}
                </TableCell>
              ))}
            </TableRow>
          </TableHead>
          <TableBody>
            {jobDetails.candidateModels.map((row, index) => {
              return (
                <TableRow hover role="checkbox" tabIndex={-1} key={index}>
                  {applicantDetailsHead.map((headCell, idx) => {
                    return (
                      <TableCell
                        className={classes.value}
                        key={idx}
                        // align="center"
                        style={{maxWidth:"100px"}}
                      >
                        {headCell.id === "chooseDemand"
                          ? getDemandForm(row, index)
                          : headCell.id === "appliedOn"
                          ? row[headCell.id].split("T")[0]
                          :row[headCell.id]}
                      </TableCell>
                    );
                  })}
                </TableRow>
              );
            })}
          </TableBody>
        </Table>
      </TableContainer>
    );
  };

  const handleChangedemandId = (candidateId, demandId) => {
    let newDemand = { ...mappedCandidateToDemand, [candidateId]: demandId };
    mapCandidateToDemand(newDemand);
    let demandObj = [];
    demandId.forEach((val, index) => demandObj.push({"id": val}))
    Axios.post(`${SERVER_URL}/mapCandidateWithDemnds`,{"candidateId": candidateId, "demands": demandObj}).then(res => console.log(res.data)).catch(err => console.log(err));
    // setAlertContent({
    //   content: `Applicant Id :${candidateId} Tagged with Demand Id :${demandId}`,
    //   type: "success",
    // });
    // // setTimeout(() => setUpdateMSG(""), 5000);
    // setMapApplicantToDemand(
    //   mapApplicantToDemand.map((a2d) => {
    //     if (a2d.candidateId === candidateId) {
    //       return { candidateId: candidateId, demandId: demandId };
    //     }
    //     return a2d;
    //   })
    // );
    // saveToDb(
    //   mapApplicantToDemand.map((a2d) => {
    //     if (a2d.candidateId === candidateId) {
    //       return { candidateId: candidateId, demandId: demandId };
    //     }
    //     return a2d;
    //   })
    // );
  };

  const handleChange = (event) => {
    setPersonName(event.target.value);
  };

  const getDemandForm = (row, index) => {
    return (
      <FormControl className={classes.formControl}>        
        <InputLabel id={`demo-mutiple-chip-label-${row.candidateId}`}>Demand Id</InputLabel>
        <Select
          labelId={`demo-mutiple-chip-label-${row.candidateId}`}
          id={`demo-mutiple-chip-${row.candidateId}`}
          multiple
          value={ row.demands.length !==0 && !(mappedCandidateToDemand[row.candidateId]) ? row.demands.map(demand => demand.id) : mappedCandidateToDemand[row.candidateId] ? mappedCandidateToDemand[row.candidateId]:[]}
          onChange={(e) => {
            handleChangedemandId(row.candidateId, e.target.value);
          }}
          input={<Input id={`select-multiple-chip-${row.candidateId}`} />}
          renderValue={(selected) => (
            <div className={classes.chips}>
              {selected.map((value) => (
                <Chip key={value} label={value} className={classes.chip} />
              ))}
            </div>
          )}
          MenuProps={MenuProps}
        >
          {jobDetails.demandGroup.demands.map((demand) => (
            <MenuItem key={demand.id} value={demand.id}>
              {demand.id}
            </MenuItem>
          ))}
        </Select>


      </FormControl>
    );
  };

  const saveToDb = (mapping) => {
    console.log("M", mapping);
    // Axios.post(`${SERVER_URL}/saveMapping`, mapping).then(
    //   (res) => {
    //     console.log(res.data);
    //   },
    //   (err) => {
    //     console.log(err);
    //   }
    // );
  };

  // const getTaggedDetails = () => {
  //   return (
  //     <TableContainer className={classes.container}>
  //       <Table
  //         className={classes.table}
  //         stickyHeader
  //         aria-labelledby="tableTitle"
  //         size="medium"
  //         aria-label="enhanced table"
  //       >
  //         <TableHead>
  //           <TableRow tabIndex={-1}>
  //             {taggedApplicationHead.map((headCell) => (
  //               <TableCell
  //                 className={classes.tablehead}
  //                 key={headCell.id}
  //                 align="center"
  //               >
  //                 {headCell.label}
  //               </TableCell>
  //             ))}
  //           </TableRow>
  //         </TableHead>
  //         <TableBody>
  //           {taggedApplicationList.map((row, index) => {
  //             return (
  //               <TableRow hover role="checkbox" tabIndex={-1} key={index}>
  //                 {taggedApplicationHead.map((headCell, idx) => {
  //                   return (
  //                     <TableCell
  //                       className={classes.value}
  //                       key={idx}
  //                       align="center"
  //                     >
  //                       {row[headCell.id]}
  //                     </TableCell>
  //                   );
  //                 })}
  //               </TableRow>
  //             );
  //           })}
  //         </TableBody>
  //       </Table>
  //     </TableContainer>
  //   );
  // };

  const handleFullScreen = () => {
    setFullScreen(!fullScreen);
  };

  const handleAlertClose = () => {
    setAlertContent({ content: "", type: "" });
  };

  return (
    <Dialog
      fullScreen={fullScreen}
      fullWidth={true}
      maxWidth="md"
      open={props.open}
      onClose={() => props.handleClose(false)}
      aria-labelledby="max-width-dialog-title"
    >
      <DialogTitle id="max-width-dialog-title">
        <Grid
          container
          direction="row"
          justify="center"
          alignItems="center"
          spacing={3}
        >
          <Grid item xs={10}>
            <Paper className={classes.headpaper}>
              <EnhancedTableToolbar id={jobDetails.jobId} />
            </Paper>
          </Grid>
          <Grid item xs={1}>
            <Button
              onClick={handleFullScreen}
              color="primary"
              variant="outlined"
            >
              {fullScreen ? "Minimize" : "Full"}
            </Button>
          </Grid>
          <Grid item xs={1}>
            <Button
              onClick={() => props.handleClose(false)}
              color="secondary"
              variant="outlined"
            >
              {fullScreen ? "Close" : "X"}
            </Button>
          </Grid>
        </Grid>
      </DialogTitle>
      <DialogContent>
        {loading ? (
          <LoadingContent />
        ) : (
          <div className={classes.root}>
            <Grid container direction="column" justify="center" spacing={2}>
              <Grid item xs={12}>
                <Paper
                  className={classes.paper}
                  onClick={() => handleClickDisplay(0)}
                >
                  <h3 style={{ paddingTop: "9px" }}>
                    Demand Behind This Opening
                  </h3>
                </Paper>
              </Grid>
              {open[0] ? (
                <Grid item xs={12}>
                  <Paper className={classes.subpaper}>
                    {getDemandDetails()}
                  </Paper>
                </Grid>
              ) : null}
              <Grid item xs={12}>
                <Paper
                  className={classes.paper}
                  onClick={() => handleClickDisplay(1)}
                >
                  <h3 style={{ paddingTop: "9px" }}>Applied Candidates</h3>
                </Paper>
              </Grid>
              {open[1] ? (
                <Grid item xs={12}>
                  <Paper className={classes.subpaper}>
                    {getApplicantDetails()}
                  </Paper>
                </Grid>
              ) : null}
              {/* <Grid item xs={12}>
                <Paper
                  className={classes.paper}
                  onClick={() => handleClickDisplay(2)}
                >
                  <h3 style={{ paddingTop: "9px" }}>Tagged Candidate</h3>
                </Paper>
              </Grid>
              {open[2] ? (
                <Grid item xs={12}>
                  <Paper className={classes.subpaper}>
                    {getTaggedDetails()}
                  </Paper>
                </Grid>
              ) : null} */}
              <Grid item xs={12}></Grid>
            </Grid>
            {alertContent.content !== "" ? (
              <AlertDialog
                open={alertContent.content !== ""}
                content={alertContent}
                close={handleAlertClose}
              />
            ) : null}
          </div>
        )}
      </DialogContent>
    </Dialog>
  );
};

export default AllocationDetails;
