import React, { useState, useEffect, Fragment } from "react";
import { makeStyles } from "@material-ui/core/styles";
import TextField from "@material-ui/core/TextField";
import {
  MenuItem,
  Toolbar,
  Grid,
  Paper,
  Button,
  Switch,
} from "@material-ui/core";
import Typography from "@material-ui/core/Typography";
import Slider from "@material-ui/core/Slider";
import Autocomplete from "@material-ui/lab/Autocomplete";
import clsx from "clsx";
import TextEditor from "./TextEditor";
import AlertDialog from "../../Common/Alert";
import LoadingContent from "../../Common/LoadingContent";
import Axios from "axios";
import { SERVER_URL } from "../../Common/StaticContent";

const useToolbarStyles = makeStyles((theme) => ({
  root: {
    paddingLeft: theme.spacing(2),
    paddingRight: theme.spacing(1),
  },
  title: {
    flex: "1 1 100%",
  },
}));

const EnhancedTableToolbar = (props) => {
  const classes = useToolbarStyles();
  return (
    <Toolbar className={clsx(classes.root)}>
      <Typography
        className={classes.title}
        variant="h6"
        id="tableTitle"
        component="div"
      >
        {props.titleSource.source + " > " + props.titleSource.subSource}
        {props.loading ? <LoadingContent /> : null}
      </Typography>
    </Toolbar>
  );
};

const useStyles = makeStyles((theme) => ({
  root: {
    flexGrow: 1,
  },
  paper: {
    padding: theme.spacing(2),
    textAlign: "center",
    color: theme.palette.text.secondary,
    height: "80px",
  },
  errorpaper: {
    padding: theme.spacing(2),
    textAlign: "center",
    color: theme.palette.text.secondary,
    height: "80px",
    background: "#ffcccc",
  },
  button: {
    textAlign: "center",
    padding: theme.spacing(1),
  },
}));

const currencies = [
  {
    value: "USD",
    label: "Dollar($)",
  },
  {
    value: "EUR",
    label: "Euro(€)",
  },
  {
    value: "INR",
    label: "Rupees(₹)",
  },
];

const CreateOpening = (props) => {
  const classes = useStyles();
  const titleSource = props.titleSource;
  const [loading, setLoading] = useState(true);
  const [jobTitle, setJobTitle] = useState("");
  const [jobLocation, setJobLocation] = useState([]);
  const [groupId, setGroupId] = useState("");
  const [postedAt, setPostedAt] = useState("Both");
  const [showSalary, setShowSalary] = useState(false);
  const [currency, setCurrency] = useState("INR");
  const [salary, setSalary] = useState([0, 0]);
  const [jobDescription, setJobDescription] = useState("");
  const [alertOpen, setAlertOpen] = useState(false);
  const [alertContent, setAlertContent] = useState({ content: "", type: "" });
  const [locationList, setLocationList] = useState([]);
  const postedAtList = ["Career Portal", "Referral Portal", "Both"];
  const [groupIdList, setGroupIdList] = useState([]);
  const [fieldError, setFieldError] = useState({
    jobTitle: false,
    groupId: false,
    jobLocation: false,
    jobDescription: false,
    salary: false,
  });

  useEffect(() => {
    if (loading) {
      const location =  Axios.get(`${SERVER_URL}/getLocation`);
      const demandGroup = Axios.get(`${SERVER_URL}/openDemand/groupDemands?status=Active`);

      let promises = [location, demandGroup];

      Axios.all(promises)
        .then(Axios.spread((...args) => {
          setLocationList(args[0].data);
          setGroupIdList([
            {locationName: "Select Group",id: 0},
            ...args[1].data
          ]);
          setLoading(false);
          }))
        .then(response => console.log(response));
    }
  }, [loading]);

  const handleChangeJobTitle = (event) => {
    setJobTitle(event.target.value);
    setFieldError({ ...fieldError, jobTitle: event.target.value === "" });
  };

  const handleChangeCurrency = (event) => {
    setCurrency(event.target.value);
  };

  const handleChangeJobDescription = (content) => {
    setJobDescription(content);
  };

  const handleChangeJobLocation = (event, loc) => {
    setJobLocation(loc);
    setFieldError({
      ...fieldError,
      jobLocation: loc.length === 0,
    });
  };

  const handleChangeGroupId = (event) => {
    setGroupId(event.target.value);
    setFieldError({
      ...fieldError,
      groupId: event.target.value === "Select Group",
    });
  };

  const handleChangePostedAt = (event) => {
    setPostedAt(event.target.value);
  };

  const handleChangeShowSalary = (event) => {
    setShowSalary(event.target.checked);
  };

  const handleChangeSalary = (event, val) => {
    setSalary(val);
    setFieldError({
      ...fieldError,
      salary: showSalary && val[0] === val[1] && val[0] === 0,
    });
  };

  const handleAlertOpen = (content, type) => {
    setAlertContent({ content: content, type: type });
    setAlertOpen(true);
  };

  const handleAlertClose = () => {
    setAlertOpen(false);
  };

  const arrayToString = (array) => {
    if (array.length === 0) {
      return "";
    }
    let str = "";
    for (let index = 0; index < array.length - 1; index++) {
      str = str + array[index] + ",";
    }
    return str + array[array.length - 1];
  };

  const onClickSubmit = () => {
    setFieldError({
      jobTitle: jobTitle === null || jobTitle === "",
      jobDescription: jobDescription === "" || jobDescription === "<p></p>\n",
      jobLocation: jobLocation.length === 0,
      groupId: groupId === "Select Group" || groupId === "",
      salary: showSalary && salary[0] === salary[1] && salary[0] === 0,
    });
    if (
      jobTitle === null ||
      jobTitle === "" ||
      jobDescription === "" ||
      jobDescription === "<p></p>\n" ||
      jobLocation.length === 0 ||
      groupId === "Select Group" ||
      groupId === "" ||
      (showSalary && salary[0] === salary[1] && salary[0] === 0)
    ) {
      return;
    } else {
      let newJoin = {
        status: "Inactive",
        jobTitle: jobTitle,
        jobLocation: arrayToString(jobLocation),
        demandGroup: {jdGroupId: groupId},
        postedAt: postedAt,
        currency: showSalary ? currency : "",
        salary: showSalary ? salary[0] + "-" + salary[1] : "0-0",
        jobDescription: jobDescription,
      };
      console.log("Create With:", newJoin);
      saveToDb(newJoin);
    }
  };

  const onClickCancel = (reset) => {
    setJobTitle("");
    setJobLocation([]);
    setGroupId("");
    setPostedAt("Both");
    setShowSalary(false);
    setCurrency("INR");
    setSalary([0, 0]);
    setJobDescription("");
    setFieldError({
      jobTitle: false,
      groupId: false,
      jobLocation: false,
      salary: false,
    });
    if (reset) {
      setAlertContent({ content: "", type: "" });
    }
  };

  const saveToDb = (object) => {
    Axios.post(`${SERVER_URL}/saveJobOpening`, object).then(
      (res) => {
        console.log(res);
        handleAlertOpen(
          `Job Added With Job ID :${res.data} In Inactive Status`,
          "success"
        );
        onClickCancel();
      },
      (err) => {
        console.log(err);
      }
    );
  };

  return (
    <div className={classes.root}>
      <Grid container spacing={3}>
        <Grid item xs={12}>
          <Paper>
            <EnhancedTableToolbar titleSource={titleSource} loading={loading} />
          </Paper>
        </Grid>
        {loading ? null : (
          <Fragment>
            <Grid item xs={9}>
              <Paper className={classes.paper}>
                <TextField
                  error={fieldError["jobTitle"]}
                  id="outlined-basic"
                  label={
                    fieldError["jobTitle"]
                      ? "*Required Field - Job Title"
                      : "Job Title"
                  }
                  value={jobTitle}
                  onChange={handleChangeJobTitle}
                  style={{ width: "100%" }}
                />
              </Paper>
            </Grid>
            <Grid item xs={3}>
              <Paper className={classes.paper}>
                <TextField
                  error={fieldError["groupId"]}
                  id="standard-select-groupId"
                  select
                  label={
                    fieldError["groupId"] ? "*Required Field" : "Select Group"
                  }
                  value={groupId === "" ? "Select Group" : groupId}
                  onChange={handleChangeGroupId}
                  style={{ width: "100%" }}
                >
                  {groupIdList.map((option, index) => (
                    <MenuItem key={index} value={option.jdGroupId}>
                      {option.jdGroupName}
                    </MenuItem>
                  ))}
                </TextField>
              </Paper>
            </Grid>
            <Grid item xs={7}>
              <Paper className={classes.paper}>
                <Autocomplete
                  multiple
                  options={locationList.map(location => location.locationName)}
                  value={jobLocation}
                  id="standard-select-jobLocation"
                  onChange={handleChangeJobLocation}
                  renderInput={(params) => (
                    <TextField
                      {...params}
                      label={
                        fieldError["jobLocation"]
                          ? "*Required Field - Job Location"
                          : "Job Location"
                      }
                      error={fieldError["jobLocation"]}
                    />
                  )}
                />
              </Paper>
            </Grid>
            <Grid item xs={3}>
              <Paper className={classes.paper}>
                <TextField
                  id="standard-select-postedAt"
                  select
                  label="Posted On"
                  value={postedAt}
                  onChange={handleChangePostedAt}
                  style={{ width: "100%" }}
                >
                  {postedAtList.map((option, index) => (
                    <MenuItem key={index} value={option}>
                      {option}
                    </MenuItem>
                  ))}
                </TextField>
              </Paper>
            </Grid>
            <Grid item xs={2}>
              <Paper className={classes.paper}>
                <h3>
                  <b>Salary</b>
                  <Switch
                    id="standard-select-showSalary"
                    checked={showSalary}
                    onChange={handleChangeShowSalary}
                    color="primary"
                    inputProps={{ "aria-label": "primary checkbox" }}
                  />
                </h3>
              </Paper>
            </Grid>
            {showSalary ? (
              <Fragment>
                <Grid item xs={4}>
                  <Paper className={classes.paper}>
                    <TextField
                      id="standard-select-currency"
                      select
                      label="Currency"
                      value={currency}
                      onChange={handleChangeCurrency}
                      style={{ width: "100%" }}
                    >
                      {currencies.map((option) => (
                        <MenuItem key={option.value} value={option.value}>
                          {option.label}
                        </MenuItem>
                      ))}
                    </TextField>
                  </Paper>
                </Grid>
                <Grid item xs={8}>
                  <Paper className={classes.paper}>
                    <Typography id="range-slider" gutterBottom>
                      {fieldError["salary"] ? (
                        <span style={{ color: "red" }}>
                          {"*Invalid Range - "}
                        </span>
                      ) : null}
                      Salary from {salary[0]}LPA to {salary[1]}LPA
                    </Typography>
                    <Slider
                      value={salary}
                      min={0.0}
                      max={20.0}
                      step={0.1}
                      onChange={handleChangeSalary}
                      valueLabelDisplay="auto"
                      aria-labelledby="range-slider"
                    />
                  </Paper>
                </Grid>
              </Fragment>
            ) : null}
            <Grid item xs={12}>
              <Typography id="range-slider" gutterBottom>
                {fieldError["jobDescription"] ? (
                  <span style={{ color: "red" }}>{"*Required Field"}</span>
                ) : null}
              </Typography>
              <TextEditor
                error={fieldError["jobDescription"]}
                reset={jobDescription === ""}
                style={{ width: "120%", textAlign: "center" }}
                onChange={handleChangeJobDescription}
              />
            </Grid>
            <Grid
              container
              direction="row"
              justify="center"
              alignItems="center"
              spacing={2}
            >
              <Grid item xs={1}>
                <Button
                  variant="contained"
                  color="primary"
                  onClick={onClickSubmit}
                >
                  Submit
                </Button>
              </Grid>
              <Grid item xs={1}>
                <Button
                  variant="contained"
                  color="secondary"
                  onClick={() => onClickCancel(true)}
                >
                  Reset
                </Button>
              </Grid>
            </Grid>
          </Fragment>
        )}
      </Grid>
      {alertOpen ? (
        <AlertDialog
          open={alertOpen}
          content={alertContent}
          close={handleAlertClose}
        />
      ) : null}
      <br />
    </div>
  );
};

export default CreateOpening;
