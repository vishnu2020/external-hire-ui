import React, { useState, useEffect, Fragment } from "react";
import clsx from "clsx";
import { makeStyles } from "@material-ui/core/styles";
import Table from "@material-ui/core/Table";
import TableBody from "@material-ui/core/TableBody";
import TableCell from "@material-ui/core/TableCell";
import TableContainer from "@material-ui/core/TableContainer";
import TableHead from "@material-ui/core/TableHead";
import TablePagination from "@material-ui/core/TablePagination";
import TableRow from "@material-ui/core/TableRow";
import TableSortLabel from "@material-ui/core/TableSortLabel";
import Toolbar from "@material-ui/core/Toolbar";
import Typography from "@material-ui/core/Typography";
import Paper from "@material-ui/core/Paper";
import EditIcon from "@material-ui/icons/Edit";
import SearchIcon from "@material-ui/icons/Search";
import { Grid, Button, TextField, InputAdornment } from "@material-ui/core";
import UpdateOpening from "./UpdateOpening";
import AlertDialog from "../../Common/Alert";
import Axios from "axios";
import { SERVER_URL } from "../../Common/StaticContent";
import LoadingContent from "../../Common/LoadingContent";

const headCells = [
  { id: "jobId", label: "Job ID", width: "short" },
  { id: "status", label: "Status", width: "short" },
  { id: "jobTitle", label: "Job Title", width: "long" },
  { id: "jobLocation", label: "Job Location", width: "long" },
  { id: "groupId", label: "Group Id", width: "long" },
  { id: "createdOn", label: "Created On", width: "short" },
  { id: "postedAt", label: "Posted At", width: "short" },
  { id: "action", label: "Action", width: "short" },
];

const useToolbarStyles = makeStyles((theme) => ({
  root: {
    paddingLeft: theme.spacing(2),
    paddingRight: theme.spacing(1),
  },
  title: {
    flex: "1 1 100%",
  },
}));

const EnhancedTableToolbar = (props) => {
  const classes = useToolbarStyles();
  const [searchText, setSearchText] = useState("");

  const handleChangeSearchText = (event) => {
    setSearchText(event.target.value);
  };
  return (
    <Grid
      container
      direction="row"
      justify="center"
      alignItems="center"
      spacing={3}
    >
      <Grid item xs={4}>
        <Toolbar className={clsx(classes.root)}>
          <Typography
            className={classes.title}
            variant="h6"
            id="tableTitle"
            component="div"
          >
            {props.titleSource
              ? props.titleSource.source + " > " + props.titleSource.subSource
              : " "}
          </Typography>
        </Toolbar>
      </Grid>
      <Grid item xs={3}>
        {props.loading ? <LoadingContent /> : null}
      </Grid>
      <Grid item xs={5}>
        <Grid
          container
          direction="row"
          justify="center"
          alignItems="flex-end"
          spacing={1}
        >
          <Grid item xs={6} style={{ height: "80%" }}>
            <TextField
              id="outlined-basic"
              placeholder="Search"
              size="small"
              value={searchText}
              onChange={handleChangeSearchText}
              style={{ width: "100%" }}
              InputProps={{
                startAdornment: (
                  <InputAdornment position="start">
                    <SearchIcon />
                  </InputAdornment>
                ),
              }}
            />
          </Grid>
          <Grid item xs={2}>
            <Button
              variant="contained"
              onClick={() => props.handleChangeSearchText(searchText)}
              style={{ background: "#85adad", color: "#fff" }}
            >
              Search
            </Button>
          </Grid>
        </Grid>
      </Grid>
    </Grid>
  );
};

const EnhancedTableHead = (props) => {
  const { classes, order, orderBy, onRequestSort } = props;
  const createSortHandler = (property) => (event) => {
    onRequestSort(event, property);
  };

  return (
    <TableHead>
      <TableRow tabIndex={-1}>
        {headCells.map((headCell) => (
          <TableCell
            className={classes.tablehead}
            key={headCell.id}
            align="center"
            sortDirection={orderBy === headCell.id ? order : false}
            onClick={createSortHandler(headCell.id)}
          >
            {headCell.label}
            {headCell.label === "" ? null : (
              <TableSortLabel
                active={orderBy === headCell.id}
                direction={orderBy === headCell.id ? order : "asc"}
              >
                {orderBy === headCell.id ? (
                  <span className={classes.visuallyHidden}>
                    {order === "desc"
                      ? "sorted descending"
                      : "sorted ascending"}
                  </span>
                ) : null}
              </TableSortLabel>
            )}
          </TableCell>
        ))}
      </TableRow>
    </TableHead>
  );
};

const useStyles = makeStyles((theme) => ({
  root: {
    width: "100%",
  },
  paper: {
    width: "100%",
    marginBottom: theme.spacing(2),
  },
  table: {
    minWidth: 750,
  },
  tablehead: {
    background: "#85adad",
    color: "#fff",
    fontSize: "120%",
  },
  container: {
    maxHeight: 400,
  },
  visuallyHidden: {
    border: 0,
    clip: "rect(0 0 0 0)",
    height: 1,
    margin: -1,
    overflow: "hidden",
    padding: 0,
    position: "absolute",
    top: 20,
    width: 1,
  },
  value: {
    paddingRight: theme.spacing(6),
  },
}));

const ViewOpenings = (props) => {
  const classes = useStyles();
  const [order, setOrder] = useState("asc");
  const [titleSource, setTitleSource] = useState(props.titleSource);
  const [orderBy, setOrderBy] = useState("jobId");
  const [page, setPage] = useState(0);
  const [rowsPerPage, setRowsPerPage] = useState(5);
  const [searchText, setSearchText] = useState("");
  const [openPopUp, setOpenPopUp] = useState(false);
  const [editObj, setEditObj] = useState({});
  const [alertOpen, setAlertOpen] = useState(false);
  const [rows, setRows] = useState([]);
  const [loading, setLoading] = useState(true);

  useEffect(() => {
    if (loading) {
      setPage(0);
      setTitleSource(props.titleSource);
      Axios.get(`${SERVER_URL}/getAllJobOpenings`).then(
        (res) => {
          // console.log(res.data)
          filterData(res.data);
          setLoading(false);
        },
        (err) => {
          console.log(err);
        }
      );
    }
  }, [props.titleSource, loading]);

  const filterData = (datas) => {
    datas = datas.filter((data) => {
      if (
        data.jobTitle.toLowerCase().includes(searchText.toLowerCase()) ||
        data.jobLocation.toLowerCase().includes(searchText.toLowerCase())
      ) {
        return true;
      }
      return false;
    });
    setRows(datas);
  };

  const handleRequestSort = (event, property) => {
    const isAsc = orderBy === property && order === "asc";
    setOrder(isAsc ? "desc" : "asc");
    setOrderBy(property);
  };

  const handleChangePage = (event, newPage) => {
    setPage(newPage);
  };

  const handleChangeRowsPerPage = (event) => {
    setRowsPerPage(parseInt(event.target.value, 10));
    setPage(0);
  };

  const handleChangeSearchText = (text) => {
    setSearchText(text);
    setLoading(true);
    // alert(text);
  };

  const handleOpenEditPopUp = (obj) => {
    setOpenPopUp(true);
    obj.jobLocation = obj.jobLocation.split(",");
    setEditObj(obj);
  };

  const handleCloseEditPopUp = (editStatus) => {
    setLoading(true);
    setOpenPopUp(false);
    setAlertOpen(editStatus);
  };

  const handleAlertClose = () => {
    setAlertOpen(false);
  };

  const descComparator = (a, b, orderBy) => {
    if (b[orderBy] < a[orderBy]) {
      return -1;
    }
    if (b[orderBy] > a[orderBy]) {
      return 1;
    }
    return 0;
  };

  const stableSort = (array) => {
    const stabilizedThis = array.map((el, index) => [el, index]);
    stabilizedThis.sort((a, b) => {
      const orderVal =
        order === "desc"
          ? descComparator(a[0], b[0], orderBy)
          : -descComparator(a[0], b[0], orderBy);
      if (orderVal !== 0) return orderVal;
      return a[1] - b[1];
    });
    return stabilizedThis.map((el) => el[0]);
  };

  const emptyRows =
    rowsPerPage - Math.min(rowsPerPage, rows.length - page * rowsPerPage);
  const rowsToDisplay = stableSort(rows).slice(
    page * rowsPerPage,
    page * rowsPerPage + rowsPerPage
  );

  return (
    <div className={classes.root}>
      <Paper className={classes.paper}>
        <EnhancedTableToolbar
          titleSource={titleSource}
          searchText={searchText}
          loading={loading}
          handleChangeSearchText={handleChangeSearchText}
        />
        {loading ? null : (
          <Fragment>
            <TableContainer className={classes.container}>
              <Table
                className={classes.table}
                stickyHeader
                aria-labelledby="tableTitle"
                size="medium"
                aria-label="enhanced table"
              >
                <EnhancedTableHead
                  classes={classes}
                  order={order}
                  orderBy={orderBy}
                  onRequestSort={handleRequestSort}
                />
                <TableBody>
                  {rowsToDisplay.map((row, index) => {
                    return (
                      <TableRow
                        hover
                        role="checkbox"
                        tabIndex={-1}
                        key={row.jobId}
                      >
                        {headCells.map((headCell, idx) => {
                          return (
                            <TableCell
                              className={classes.value}
                              key={idx}
                              align="center"
                            >
                              {headCell.id === "action" ? (
                                <EditIcon
                                  onClick={() => {
                                    handleOpenEditPopUp(row);
                                  }}
                                />
                              ) : headCell.id === "createdOn" ? (
                                row[headCell.id].split("T")[0]
                              ) : (
                                row[headCell.id]
                              )}
                            </TableCell>
                          );
                        })}
                      </TableRow>
                    );
                  })}
                  {emptyRows > 0 && (
                    <TableRow style={{ height: 53 * emptyRows }}>
                      <TableCell colSpan={6} />
                    </TableRow>
                  )}
                </TableBody>
              </Table>
            </TableContainer>
            {openPopUp ? (
              <UpdateOpening
                open={openPopUp}
                handleClose={handleCloseEditPopUp}
                editRowValue={editObj}
              />
            ) : null}
            {alertOpen ? (
              <AlertDialog
                open={alertOpen}
                content={{
                  content: `Job ID : ${editObj.jobId} Updated Successfully`,
                  type: "success",
                }}
                close={handleAlertClose}
              />
            ) : null}
            <TablePagination
              rowsPerPageOptions={[3, 5, 10, 25]}
              component="div"
              count={rows.length}
              rowsPerPage={rowsPerPage}
              page={page}
              onChangePage={handleChangePage}
              onChangeRowsPerPage={handleChangeRowsPerPage}
            />
          </Fragment>
        )}
      </Paper>
    </div>
  );
};

export default ViewOpenings;
