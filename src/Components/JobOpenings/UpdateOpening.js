import React, { useState, Fragment, useEffect } from "react";
import { makeStyles } from "@material-ui/core/styles";
import Button from "@material-ui/core/Button";
import Dialog from "@material-ui/core/Dialog";
import DialogActions from "@material-ui/core/DialogActions";
import DialogContent from "@material-ui/core/DialogContent";
import DialogTitle from "@material-ui/core/DialogTitle";
import TextEditor from "./TextEditor";
import {
  Grid,
  TextField,
  MenuItem,
  Switch,
  Typography,
  Slider,
  Paper,
  Toolbar,
} from "@material-ui/core";
import Autocomplete from "@material-ui/lab/Autocomplete";
import clsx from "clsx";
import Axios from "axios";
import { SERVER_URL } from "../../Common/StaticContent";
import AlertDialog from "../../Common/Alert";

const useToolbarStyles = makeStyles((theme) => ({
  root: {
    paddingLeft: theme.spacing(2),
    paddingRight: theme.spacing(1),
  },
  title: {
    flex: "1 1 100%",
  },
  paper: {
    padding: theme.spacing(2),
    textAlign: "center",
    color: theme.palette.text.secondary,
    height: "60px",
  },
}));

const EnhancedTableToolbar = (props) => {
  const classes = useToolbarStyles();
  return (
    <Toolbar className={clsx(classes.root)}>
      <Grid
        container
        direction="row"
        justify="center"
        alignItems="center"
        spacing={3}
      >
        <Grid item xs={8}>
          <Typography
            className={classes.title}
            variant="h6"
            id="tableTitle"
            component="div"
          >
            {"Job Openings > View Openings > Edit Opening - ID : " +
              props.jobId}
          </Typography>
        </Grid>
        <Grid item xs={3}>
          <Paper className={classes.paper}>
            <b>Active Status :</b>
            <Switch
              id="standard-select-status"
              checked={props.status}
              onChange={props.handleChange}
              color="secondary"
              inputProps={{ "aria-label": "primary checkbox" }}
            />
          </Paper>
        </Grid>
        <Grid item xs={1}></Grid>
      </Grid>
    </Toolbar>
  );
};

const useStyles = makeStyles((theme) => ({
  root: {
    flexGrow: 1,
  },
  paper: {
    padding: theme.spacing(2),
    textAlign: "center",
    color: theme.palette.text.secondary,
    height: "80px",
  },
  errorpaper: {
    padding: theme.spacing(2),
    textAlign: "center",
    color: theme.palette.text.secondary,
    height: "80px",
    background: "#ffcccc",
  },
  button: {
    textAlign: "center",
    padding: theme.spacing(1),
  },
}));

const currencies = [
  {
    value: "USD",
    label: "Dollar($)",
  },
  {
    value: "EUR",
    label: "Euro(€)",
  },
  {
    value: "INR",
    label: "Rupees(₹)",
  },
];

const UpdateOpening = (props) => {
  const { editRowValue } = props;
  const prev_jobDescription = editRowValue.jobDescription || "";
  const classes = useStyles();
  const [loading, setLoading] = useState(true);
  const [status, setStatus] = useState(editRowValue.status === "Active");
  const [jobTitle, setJobTitle] = useState(editRowValue.jobTitle);
  const [jobLocation, setJobLocation] = useState(editRowValue.jobLocation);
  const [groupId, setGroupId] = useState(editRowValue.demandGroup.jdGroupId);
  const [postedAt, setPostedAt] = useState(editRowValue.postedAt);
  const [showSalary, setShowSalary] = useState(editRowValue.currency !== "");
  const [currency, setCurrency] = useState(
    showSalary ? editRowValue.currency : "INR"
  );
  const [salary, setSalary] = useState(
    showSalary ? editRowValue.salary.split("-") : [0.0, 0.0]
  );
  const [jobDescription, setJobDescription] = useState(
    editRowValue.jobDescription !== undefined ? editRowValue.jobDescription : ""
  );
  const [alertContent, setAlertContent] = useState({ content: "", type: "" });
  const [jobLocationList, setJobLocationList] = useState([]);
  const [groupIdList, setGroupIdList] = useState([]);
  const postedAtList = ["Career Portal", "Referral Portal", "Both"];
  const [fieldError, setFieldError] = useState({
    jobTitle: false,
    groupId: false,
    jobLocation: false,
    salary: false,
  });

  useEffect(() => {
    if (loading) {

      const location =  Axios.get(`${SERVER_URL}/getLocation`);
      const demandGroup = Axios.get(`${SERVER_URL}/openDemand/groupDemands?status=Active`);

      let promises = [location, demandGroup];

      Axios.all(promises)
        .then(Axios.spread((...args) => {
          console.log("args");
          console.log(args);
          setJobLocationList(args[0].data);
          setGroupIdList([
            {locationName: "Select Group",id: 0},
            ...args[1].data
          ]);
          setLoading(false);
          }))
        .then(response => console.log(response));

      if (jobLocationList.length !== 0) {
        setLoading(false);
      }
    } else {
      editFromValidation();
    }
  }, [loading, jobTitle, groupId, jobLocation, showSalary, salary]);

  const handleChangeStatus = (event) => {
    setStatus(event.target.checked);
  };
  const handleChangeJobTitle = (event) => {
    setJobTitle(event.target.value);
    setFieldError({ ...fieldError, jobTitle: event.target.value === "" });
  };

  const handleChangeCurrency = (event) => {
    setCurrency(event.target.value);
  };

  const handleChangeJobDescription = (content) => {
    setJobDescription(content);
  };

  const handleChangeLocation = (event, loc) => {
    setJobLocation(loc);
    setFieldError({
      ...fieldError,
      jobLocation: loc.length === 0,
    });
  };

  const handleChangeGroupId = (event) => {
    setGroupId(event.target.value);
    setFieldError({
      ...fieldError,
      groupId: event.target.value === "Select Group",
    });
  };

  const handleChangePostedAt = (event) => {
    setPostedAt(event.target.value);
  };

  const handleChangeShowSalary = (event) => {
    setShowSalary(event.target.checked);
  };

  const handleChangeSalary = (event, val) => {
    setSalary(val);
    setFieldError({
      ...fieldError,
      salary: showSalary && val[0] === val[1] && val[0] === 0,
    });
  };

  const handleAlertClose = () => {
    setAlertContent({ content: "", type: "" });
  };

  const editFromValidation = () => {
    let nullFields = [];
    let fieldsToCheck = [
      { label: "Job Title", value: jobTitle },
      { label: "Location", value: jobLocation },
      { label: "Group Ids", value: groupId },
      { label: "Job Description", value: jobDescription },
    ];
    fieldsToCheck.forEach((field) => {
      if (field.label === "Location" && field.value.length === 0) {
        nullFields.push(field.label);
      } else if (
        field.label === "Job Description" &&
        (field.value === "" || field.value === "<p></p>\n")
      ) {
        setJobDescription(prev_jobDescription);
      } else if (
        field.label === "Group Ids" &&
        field.value === "Select Group"
      ) {
        nullFields.push(field.label);
      } else if (field.value === null || field.value === "") {
        nullFields.push(field.label);
      }
    });
    if (nullFields.length > 0) {
      nullFields = nullFields.map((nf) => {
        return nf;
      });
      setAlertContent({
        content: "Please fill " + nullFields,
        type: "warning",
      });
      return false;
    } else if (showSalary && salary[0] === salary[1] && salary[0] === 0) {
      setAlertContent({
        content: "Please select a valid salary range!!!",
        type: "warning",
      });
      return false;
    }
    setAlertContent({ content: "", type: "" });
    return true;
  };

  const onClickSubmit = () => {
    if (editFromValidation()) {
      let updatedJoin = {
        jobId: editRowValue.jobId,
        status: status ? "Active" : "Inactive",
        jobTitle: jobTitle,
        jobLocation: arrayToString(jobLocation),
        postedAt: postedAt,
        currency: showSalary ? currency : "",
        salary: showSalary ? salary[0] + "-" + salary[1] : "0-0",
        jobDescription: jobDescription,
        createdOn: editRowValue.createdOn,
        updatedOn: editRowValue.updatedOn,
        demandGroup: {jdGroupId: groupId},
      };
      console.log("Update With:", updatedJoin);
      updateToDb(updatedJoin);
      // props.handleClose(true);
    }
  };

  const updateToDb = (object) => {
    Axios.post(`${SERVER_URL}/updateJobOpening`, object).then(
      (res) => {
        props.handleClose(true);
      },
      (err) => {
        console.log(err);
      }
    );
  };
  const arrayToString = (array) => {
    // console.log(array);
    if (array.length === 0) {
      return "";
    }
    let str = "";
    for (let index = 0; index < array.length - 1; index++) {
      str = str + array[index] + ",";
    }
    return str + array[array.length - 1];
  };

  const [inputValueLocation, setInputValueLocation] = useState(
    arrayToString(editRowValue.jobLocation)
  );

  return (
    <Dialog
      fullWidth={true}
      maxWidth="lg"
      open={props.open}
      onClose={() => props.handleClose(false)}
      aria-labelledby="max-width-dialog-title"
    >
      <DialogTitle id="max-width-dialog-title">
        <EnhancedTableToolbar
          jobId={props.editRowValue.jobId}
          status={status}
          handleChange={handleChangeStatus}
        />
      </DialogTitle>
      <DialogContent>
        <div className={classes.root}>
          <Grid container spacing={3}>
            <Grid item xs={9}>
              <Paper className={classes.paper}>
                <TextField
                  error={fieldError["jobTitle"]}
                  id="outlined-basic"
                  label={
                    fieldError["jobTitle"]
                      ? "*Required Field - Job Title"
                      : "Job Title"
                  }
                  value={jobTitle}
                  onChange={handleChangeJobTitle}
                  style={{ width: "100%" }}
                />
              </Paper>
            </Grid>
            <Grid item xs={3}>
              <Paper className={classes.paper}>
                <TextField
                  error={fieldError["groupId"]}
                  id="standard-select-groupId"
                  select
                  label={
                    fieldError["groupId"] ? "*Required Field" : "Select Group"
                  }
                  value={groupId === "" ? "Select Group" : groupId}
                  onChange={handleChangeGroupId}
                  style={{ width: "100%" }}
                >
                  {groupIdList.map((option, index) => (
                    <MenuItem key={index} value={option.jdGroupId}>
                      {option.jdGroupName}
                    </MenuItem>
                  ))}
                </TextField>
              </Paper>
            </Grid>
            <Grid item xs={7}>
              <Paper className={classes.paper}>
                <Autocomplete
                  multiple
                  options={jobLocationList.map(location => location.locationName)}
                  value={jobLocation}
                  id="standard-select-jobLocation"
                  inputValue={inputValueLocation}
                  onInputChange={(e, nv) =>
                    setInputValueLocation(arrayToString(nv))
                  }
                  onChange={handleChangeLocation}
                  renderInput={(params) => (
                    <TextField
                      {...params}
                      label={
                        fieldError["jobLocation"]
                          ? "*Required Field - Job Location"
                          : "Job Location"
                      }
                      error={fieldError["jobLocation"]}
                    />
                  )}
                />
              </Paper>
            </Grid>
            <Grid item xs={3}>
              <Paper className={classes.paper}>
                <TextField
                  id="standard-select-postedAt"
                  select
                  label="Posted On"
                  value={postedAt}
                  onChange={handleChangePostedAt}
                  style={{ width: "100%" }}
                >
                  {postedAtList.map((option, index) => (
                    <MenuItem key={index} value={option}>
                      {option}
                    </MenuItem>
                  ))}
                </TextField>
              </Paper>
            </Grid>
            <Grid item xs={2}>
              <Paper className={classes.paper}>
                <h3>
                  <b>Salary</b>
                  <Switch
                    id="standard-select-showSalary"
                    checked={showSalary}
                    onChange={handleChangeShowSalary}
                    color="primary"
                    inputProps={{ "aria-label": "primary checkbox" }}
                  />
                </h3>
              </Paper>
            </Grid>
            {showSalary ? (
              <Fragment>
                <Grid item xs={4}>
                  <Paper className={classes.paper}>
                    <TextField
                      id="standard-select-currency"
                      select
                      label="Currency"
                      value={currency}
                      onChange={handleChangeCurrency}
                      style={{ width: "100%" }}
                    >
                      {currencies.map((option) => (
                        <MenuItem key={option.value} value={option.value}>
                          {option.label}
                        </MenuItem>
                      ))}
                    </TextField>
                  </Paper>
                </Grid>
                <Grid item xs={8}>
                  <Paper className={classes.paper}>
                    <Typography id="range-slider" gutterBottom>
                      {fieldError["salary"] ? (
                        <span style={{ color: "red" }}>
                          {"*Invalid Range - "}
                        </span>
                      ) : null}
                      Salary from {salary[0]}LPA to {salary[1]}LPA
                    </Typography>
                    <Slider
                      value={salary}
                      min={0.0}
                      max={20.0}
                      step={0.1}
                      onChange={handleChangeSalary}
                      valueLabelDisplay="auto"
                      aria-labelledby="range-slider"
                    />
                  </Paper>
                </Grid>
              </Fragment>
            ) : null}
            {prev_jobDescription === "" ||
            prev_jobDescription === "<p></p>" ? null : (
              <Grid item xs={12}>
                <Paper className={classes.paper}>
                  <h3
                    dangerouslySetInnerHTML={{ __html: prev_jobDescription }}
                  />
                </Paper>
              </Grid>
            )}
            <Grid item xs={12}>
              <TextEditor
                style={{ width: "120%", textAlign: "center" }}
                onChange={handleChangeJobDescription}
                value={jobDescription}
              />
            </Grid>
          </Grid>
          {alertContent.content !== "" ? (
            <AlertDialog
              open={alertContent.content !== ""}
              content={alertContent}
              close={handleAlertClose}
            />
          ) : null}
          <br />
        </div>
      </DialogContent>
      <DialogActions>
        <Grid
          container
          direction="row"
          justify="center"
          alignItems="center"
          spacing={2}
        >
          <Grid item xs={1}>
            <Button variant="contained" color="primary" onClick={onClickSubmit}>
              Submit
            </Button>
          </Grid>
          <Grid item xs={1}>
            <Button
              onClick={() => props.handleClose(false)}
              color="secondary"
              variant="contained"
            >
              Close
            </Button>
          </Grid>
        </Grid>
      </DialogActions>
    </Dialog>
  );
};

export default UpdateOpening;
