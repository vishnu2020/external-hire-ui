import React,{Component} from 'react'
import { AgGridReact } from 'ag-grid-react';

import 'ag-grid-community/dist/styles/ag-grid.css';
import 'ag-grid-community/dist/styles/ag-theme-alpine.css';

class ConfirmationTable extends Component {
  constructor(props) {
    super(props);
    console.log(props)
    this.state = {
      columnDefs: [
        { headerName: "Id", field: "candidateId", filter: true, width:100 },
        { headerName: "Candidate Name", field: "firstName", filter: true, width:200},
        { headerName: "Phone Number", field: "phoneNumber", filter: true, width:200},
        { headerName: "Gender", field: "gender", filter: true, width:200}],
      rowData: this.props.selectedData,
    }
  } 

  render() {
    return (
      <div className="ag-theme-alpine" style={ {height: '400px', width: '600px'} }>
        <AgGridReact
            columnDefs={this.state.columnDefs}
            rowData={this.state.rowData}>
        </AgGridReact>
      </div>
    );
  }
}

export default ConfirmationTable;