import React, { useEffect, useState } from 'react';
import PropTypes from 'prop-types';
import { makeStyles } from '@material-ui/core/styles';
import AppBar from '@material-ui/core/AppBar';
import Tabs from '@material-ui/core/Tabs';
import Tab from '@material-ui/core/Tab';
import Typography from '@material-ui/core/Typography';
import Box from '@material-ui/core/Box';
import AssignDemand from "./AssignDemand";
import Axios from 'axios'
import { Grid, CircularProgress, Button } from '@material-ui/core';
import { CloudUploadOutlined } from '@material-ui/icons';
import AlertDialog from '../../Common/Alert';
import Notifications from '../../Common/Notifications';
import OpenDemand from './ActiveDemand';

function TabPanel(props) {
  const { children, value, index, ...other } = props;

  return (
    <div
      role="tabpanel"
      hidden={value !== index}
      id={`simple-tabpanel-${index}`}
      aria-labelledby={`simple-tab-${index}`}
      {...other}
    >
      {value === index && (
        <Box p={3}>
          <Typography>{children}</Typography>
        </Box>
      )}
    </div>
  );
}

TabPanel.propTypes = {
  children: PropTypes.node,
  index: PropTypes.any.isRequired,
  value: PropTypes.any.isRequired,
};

function a11yProps(index) {
  return {
    id: `simple-tab-${index}`,
    'aria-controls': `simple-tabpanel-${index}`,
  };
}

const useStyles = makeStyles((theme) => ({
  root: {
    flexGrow: 1,
    backgroundColor: theme.palette.background.paper,
  },
}));

export default function CustomTab() {
  const classes = useStyles();
  const [value, setValue] = useState(0);
  const [rowData, setRowData] =useState([]);
  const [fileUploadProgress, setFileUploadProgress] = useState(false);
  const [notification, setNotification] = useState({show: false, message: "", type:""});
  const [demandTabs, setDemandTabs] = useState([]);
  const [refresh, setRefresh] = useState(false);

  const handleChange = (event, newValue) => {
    console.log(newValue);
    setValue(newValue);
  };

  useEffect(() => {
    Axios.get('http://localhost:8080/openDemand/demand').then(res => {
    setRefresh(!refresh)
    setRowData(res.data)
    if(localStorage.getItem("isAdmin") === 'true'){
      setDemandTabs(demandTabsAndItsName)
    }else{
      demandTabsAndItsName.shift();
      setDemandTabs(demandTabsAndItsName)
    }
  }).catch(err => console.log(err))
  }, [refresh])

  const selectFile = (e) => {
    // console.log(e.target.files[0])
    setFileUploadProgress(true);
    var formData = new FormData();
    formData.append('demand', e.target.files[0])
    Axios.post('http://localhost:8080/openDemand/csvUpload', formData, {
      headers: {
        'Content-Type': 'multipart/form-data'
      }
    })
    .then(() => {
      setFileUploadProgress(false)
      setRefresh(!refresh);
      setNotification({ show: true, message: "Successfully uploaded excel file", type:"success"});
      setValue(0)
    })
    .catch(err => setNotification({ show: true, message: `${err.message}`, type:"error"}))
  }

  const setNotificationToState = (notification) => {
    setNotification(notification)
  }

  const closeNotification = () => {
    setNotification({
      show:false,
      message: "",
      type:""
    })
  }

  const demandTabsAndItsName = [
    {
      label: "Assign",
      element: <AssignDemand setNotificationToState={setNotificationToState} data={rowData}/>
    },
    {
      label: "Open",
      element: <OpenDemand setNotificationToState={setNotificationToState} data={rowData}/>
    },
    {
      label: "Active",
      element: ""
    },
    {
      label: "Inactive",
      element: ""
    },
  ]

  return (
    <>
      <Notifications message={notification.message} type={notification.type} open={notification.show} closeFn={closeNotification}/>
      <div className={classes.root}>
        <AppBar color="default" position="static">
          <Tabs value={value} onChange={handleChange} aria-label="simple tabs example">
            {
              demandTabs.map((demand, index) => (
                <Tab key={index} label={demand.label} {...a11yProps(index)} />
                ))
            }
            <Button
              variant="text"
              component="label"
            >
              {fileUploadProgress ? <CircularProgress size={25}/> : <CloudUploadOutlined/>}
              <div style={{paddingLeft:"5px"}}>Excel</div>
              <input
                type="file"
                style={{ display: "none" }}
                onChange={selectFile}
              />
            </Button>
          </Tabs>
        </AppBar>
        {
          demandTabs.map((demand, index) => (
            <TabPanel key={index} value={value} index={index}>
              {demand.element}
            </TabPanel>
          ))
        }
      </div>
    </>
  );
}
