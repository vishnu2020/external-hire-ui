import React, { Component } from 'react';
import HistoryIcon from '@material-ui/icons/History';
import {Button} from '@material-ui/core'

export default class HistoryIconRenderer extends Component {
  constructor(props) {
    super(props);
  }

  invokeParentMethod = () => {
      // console.log(this.props)
    this.props.context.componentParent.methodFromParent(
      this.props.data
    );
  }

  isEnabled = () => {
    if("candidateModel" in this.props.data){
      if(this.props.data.candidateModel.length === 0){
        return true;
      }
    }else
      return false
  }

  render() {
    return (
      <Button disabled={this.isEnabled()} onClick={this.invokeParentMethod}><HistoryIcon/></Button>
    );
  }
}