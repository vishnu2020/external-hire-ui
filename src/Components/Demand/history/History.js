// import React,{ useEffect, useState } from 'react';
// import PropTypes from 'prop-types';
// import { makeStyles } from '@material-ui/core/styles';
// import Box from '@material-ui/core/Box';
// import Collapse from '@material-ui/core/Collapse';
// import IconButton from '@material-ui/core/IconButton';
// import Table from '@material-ui/core/Table';
// import TableBody from '@material-ui/core/TableBody';
// import TableCell from '@material-ui/core/TableCell';
// import TableContainer from '@material-ui/core/TableContainer';
// import TableHead from '@material-ui/core/TableHead';
// import TableRow from '@material-ui/core/TableRow';
// import Typography from '@material-ui/core/Typography';
// import Paper from '@material-ui/core/Paper';
// import KeyboardArrowDownIcon from '@material-ui/icons/KeyboardArrowDown';
// import KeyboardArrowUpIcon from '@material-ui/icons/KeyboardArrowUp';
// import Axios from 'axios';

// const useRowStyles = makeStyles({
//   root: {
//     '& > *': {
//       borderBottom: 'unset',
//     },
//   },
// });

// function createData(name, calories, fat, carbs, protein, price) {
//   return {
//     name,
//     calories,
//     fat,
//     carbs,
//     protein,
//     price,
//     history: [
//       { date: '2020-01-05', customerId: '11091700', amount: 3 },
//       { date: '2020-01-02', customerId: 'Anonymous', amount: 1 },
//     ],
//   };
// }

// function Row(props) {
//   const { row } = props;
//   const [open, setOpen] = React.useState(false);
//   const classes = useRowStyles();

//   return (
//     <React.Fragment>
//       <TableRow className={classes.root}>
//         <TableCell>
//           <IconButton aria-label="expand row" size="small" onClick={() => setOpen(!open)}>
//             {open ? <KeyboardArrowUpIcon /> : <KeyboardArrowDownIcon />}
//           </IconButton>
//         </TableCell>
//         <TableCell component="th" scope="row">
//           {row.requestId}
//         </TableCell>
//         <TableCell align="right">{row.role}</TableCell>
//         <TableCell align="right">{row.accountName}</TableCell>
//         <TableCell align="right">{row.opportunity}</TableCell>
//         <TableCell align="right">{row.experience}</TableCell>
//       </TableRow>
//       <TableRow>
//         <TableCell style={{ paddingBottom: 0, paddingTop: 0 }} colSpan={6}>
//           <Collapse in={open} timeout="auto" unmountOnExit>
//             <Box margin={1}>
//               <Typography variant="h6" gutterBottom component="div">
//                 History
//               </Typography>
//               <Table size="small" aria-label="purchases">
//                 <TableHead>
//                   <TableRow>
//                     <TableCell>Date</TableCell>
//                     <TableCell>Customer</TableCell>
//                     <TableCell align="right">Amount</TableCell>
//                     <TableCell align="right">Total price ($)</TableCell>
//                   </TableRow>
//                 </TableHead>
//                 <TableBody>
//                   {/* {row.map((historyRow) => ( */}
//                     <TableRow key={row.requestId}>
//                       <TableCell component="th" scope="row">
//                         {row.role}
//                       </TableCell>
//                       <TableCell>{row.opportunity}</TableCell>
//                       <TableCell align="right">{row.experience}</TableCell>
//                       <TableCell align="right">
//                         some value
//                       </TableCell>
//                     </TableRow>
//                   {/* ))} */}
//                 </TableBody>
//               </Table>
//             </Box>
//           </Collapse>
//         </TableCell>
//       </TableRow>
//     </React.Fragment>
//   );
// }

// Row.propTypes = {
//   row: PropTypes.shape({
//     calories: PropTypes.number.isRequired,
//     carbs: PropTypes.number.isRequired,
//     fat: PropTypes.number.isRequired,
//     history: PropTypes.arrayOf(
//       PropTypes.shape({
//         amount: PropTypes.number.isRequired,
//         customerId: PropTypes.string.isRequired,
//         date: PropTypes.string.isRequired,
//       }),
//     ).isRequired,
//     name: PropTypes.string.isRequired,
//     price: PropTypes.number.isRequired,
//     protein: PropTypes.number.isRequired,
//   }).isRequired,
// };

// // const rows = [
// //   createData('Frozen yoghurt', 159, 6.0, 24, 4.0, 3.99),
// //   createData('Ice cream sandwich', 237, 9.0, 37, 4.3, 4.99),
// //   createData('Eclair', 262, 16.0, 24, 6.0, 3.79),
// //   createData('Cupcake', 305, 3.7, 67, 4.3, 2.5),
// //   createData('Gingerbread', 356, 16.0, 49, 3.9, 1.5),
// // ];

// export default function History() {

// const [rows, setRows] = useState([]);

// useEffect(() => {
//     Axios.get("http://localhost:8080/openDemand/demand")
//     .then(res => setRows(res.data))
//     .catch(err => console.log(err))
// }, [])

//   return (
//     <TableContainer component={Paper}>
//       <Table aria-label="collapsible table">
//         <TableHead>
//           <TableRow>
//             <TableCell />
//             <TableCell>Request ID</TableCell>
//             <TableCell align="right">Role</TableCell>
//             <TableCell align="right">Account Name</TableCell>
//             <TableCell align="right">Opportunity</TableCell>
//             <TableCell align="right">Experience</TableCell>
//           </TableRow>
//         </TableHead>
//         <TableBody>
//           {rows.map((row) => (
//             <Row key={row.id} row={row} />
//           ))}
//         </TableBody>
//       </Table>
//     </TableContainer>
//   );
// }

import React, {Component} from 'react';
import { AgGridReact } from 'ag-grid-react';

import 'ag-grid-community/dist/styles/ag-grid.css';
import 'ag-grid-community/dist/styles/ag-theme-alpine.css';
import Axios from 'axios';
import { Button, Grid, Paper, Input, CircularProgress } from '@material-ui/core';
import { CloudUploadOutlined, CloseOutlined } from '@material-ui/icons';
import FormHelperText from '@material-ui/core/FormHelperText';
import FormControl from '@material-ui/core/FormControl';
import Select from '@material-ui/core/Select';
import InputLabel from '@material-ui/core/InputLabel';
import {TextField, Typography, FormControlLabel, Checkbox} from '@material-ui/core';
import CommonModal from '../../../Common/CommonModal';
import DemandHistoryTable from '../DemandHistoryTable'
import { act } from 'react-dom/test-utils';
import HistoryIcon from '@material-ui/icons/History';
import HistoryIconRenderer from './icons/HistoryIconRender';

export default class Demand extends Component {
  constructor(props) {
    super(props);
    this.state = {
      gridApi: null,
      open:false,
      selectedData:null,
      progress:false,
      recruiter:null,
      fileUploadProgress:false,
      isRecruiterError: false,
      isEmptyJobSelection: false,
      errorMessage: "",
      columnDefs: [
        { headerName: "Employee Name", field: "employeeName" , filter: true, width: 300},
        { headerName: "Employee Id", field: "empId", sortable: true, filter: true, width: 300},
        { headerName: "Actions", cellRenderer: "historyIcon",colId: 'params',width: 100,}
        ],
      rowData: null,
      context: { componentParent: this },
      frameworkComponents: { historyIcon: HistoryIconRenderer }
    }
  }

  componentDidMount() {
    Axios.get("http://localhost:8080/openDemand/recruiterWithDemand")
    .then(res => this.setState({rowData: res.data}))
    .catch(err => console.log(err))
  }

  // onClickAssign = e => {
  //   if(this.state.recruiter === null)
  //   {
  //     this.setState({
  //       isRecruiterError: true,
  //       errorMessage: "Select a recruiter to proceed"
  //     })
  //     return;
  //   }
  //   const selectedNodes = this.gridApi.getSelectedNodes()
  //   if(selectedNodes.length === 0){
  //     this.setState({
  //       isEmptyJobSelection: true,
  //       errorMessage: "Select atleast one demand to proceed"
  //     })
  //     return;
  //   }
  //   const selectedData = selectedNodes.map( node => node.data )
  //   this.setState({ selectedData: selectedData, recruiter: this.state.recruiter})
  //   this.setState({open:true})
  // }

  setRecruiter = (data) => {
    this.setState({
      recruiter:data,
      isRecruiterError: false,
      errorMessage: ""
    })
  }

  confirmAssignment = () => {
    this.setState({
      progress: true
    })
  }

  closeHistoryTab = () => {
    this.refs.agGrid.api.deselectAll()
    this.setState({
      open: false
    })
  }

  rowSelected = () => {
    this.setState({
      isEmptyJobSelection: false,
      errorMessage: ""
    })
  }

  methodFromParent = cell => {
    // alert(JSON.stringify(cell.demands));
    this.setState({
      open: true,
      selectedData: cell
    })
  };

  onFirstDataRendered = params => {
    params.api.sizeColumnsToFit();
  };

  render() {

    return (
      <>
      <CommonModal style={{width:"100%"}} open={this.state.open}>
        <Paper style={{padding:"40px",width:"100%"}} elevation={3}>
          <Typography variant="h6" gutterBottom>
            History
          </Typography>
          <DemandHistoryTable selectedData={this.state.selectedData}/>
          <Grid style={{textAlign:"right"}} container>
            <Grid item xs={12}>
              <Button disabled={this.state.progress} variant="outlined" onClick={this.closeHistoryTab}>
                Close
              </Button>
            </Grid>
          </Grid>
        </Paper>
      </CommonModal>
      <Grid container spacing={1}>
        <Grid item xs={12}>
          <div className="ag-theme-alpine" style={ {height: '450px', width: '100%'} }>
            <AgGridReact
                onGridReady={ params => this.gridApi = params.api }
                rowSelection="multiple"
                columnDefs={this.state.columnDefs}
                rowData={this.state.rowData}
                animateRows
                ref="agGrid"
                onSelectionChanged={this.rowSelected}
                context={this.state.context}
                frameworkComponents={this.state.frameworkComponents}
                onFirstDataRendered={this.onFirstDataRendered.bind(this)}
                >
            </AgGridReact>
          {this.state.isEmptyJobSelection ? <p style={{color:"red"}}>{this.state.errorMessage}</p> : ""}
          </div>
        </Grid>
      </Grid>
      </>
    );
  }
}
