import React,{Component} from 'react'
import { AgGridReact } from 'ag-grid-react';

import 'ag-grid-community/dist/styles/ag-grid.css';
import 'ag-grid-community/dist/styles/ag-theme-alpine.css';

import DeleteIconRender from './Icons/DeleteIconRender';
import { Grid, Button, CircularProgress, FormControl, InputLabel, Select } from '@material-ui/core';
import Axios from 'axios';

class JDGroupConfirmationTable extends Component {
  constructor(props) {
    super(props);
    this.state = {
      columnDefs: [
        { headerName: "Request Id", field: "requestId", filter: true },
        { headerName: "Experience", field: "experience", filter: true },
        { headerName: "Hiring Status", field: "hiringStatus", width: 100 , filter: true },
        { headerName: "Role", field: "role" , filter: true },
        { headerName: "Acc Name", field: "accountName", filter: true },
        { headerName: "Action", cellRenderer: "deleteIcon", width: 60},
    ],
    columnDefsBckup:[],
    rowDataBckup: [],
      rowData: this.props.selectedData,
      gridApi: null,
      context: { componentParent: this },
      frameworkComponents: { deleteIcon: DeleteIconRender },
      progress: false,
      addMoreFlag: false,
      hiringStatus: "hold",
    }
  }

  onFirstDataRendered = params => {
    params.api.sizeColumnsToFit();
  };

  methodFromParent = cell => {
    this.setState({
      rowData: this.state.rowData.filter(row => row.id !== cell.id)
    })
  };

  closeGrouping = (msg) => {
      this.props.closeGrouping(msg);
  }

  createGroup = () => {
    this.setState({ progress: true})
    Axios.post("http://localhost:8080/openDemand/groupDemand",
        { 
            demands: this.state.rowData
        }
    ).then(res =>{
      this.setState({progress: false})
      this.closeGrouping({show: true, message: `Successfully created group ${res.data.jdGroupName}`, type:"success"});
    })
    .catch(err => this.closeGrouping({show: true, message: err.message, type:"error"}))

  }

  updateDemandGroup = () => {
    this.setState({ progress: true})
    Axios.post("http://localhost:8080/openDemand/updateGroupDemand",
        { ...this.props.selectedGroup, demands: this.state.rowData.map(demand => ({...demand, grouped:true})), }
    ).then(res =>{
      this.setState({progress: false})
      this.closeGrouping({show: true, message: `Successfully updated group ${res.data.jdGroupName}`, type:"success"});
    })
    .catch(err => this.closeGrouping({show: true, message: err.message, type:"error"}))

  }

  /**
   * Update hiring status of selected demands
   */
  updateHiringStatus = () => {
    let requestBody = []
    if(this.state.hiringStatus === "fullfilled" || this.state.hiringStatus === "cancelled")
      requestBody = this.state.rowData.map(row => ({...row, hiringStatus: this.state.hiringStatus, status:"Inactive"}))
    else
      requestBody = this.state.rowData.map(row => ({...row, hiringStatus: this.state.hiringStatus}))
    this.setState({ progress: true})
    Axios.post(`http://localhost:8080/openDemand/updateHiringStatus`,
      requestBody

    ).then(res =>{
      this.setState({progress: false})
      this.closeGrouping({show: true, message: `Successfully update hiring status of ${this.state.rowData.length} demands`, type:"success"});
    })
    .catch(err => this.closeGrouping({show: true, message: err.message, type:"error"}))
  }

  addMoreToGroup = () => {
    if(!this.state.addMoreFlag){
      this.setState({
        columnDefsBckup: this.state.columnDefs,
        columnDefs:this.props.openDemandsHeader,
        rowData:this.props.openDemands,
        rowDataBckup: this.state.rowData,
        addMoreFlag: true,
      })
    }else {
      const selectedNodes = this.gridApi.getSelectedNodes()
      const selectedData = selectedNodes.map( node => node.data )
      this.setState({
        columnDefs:this.state.columnDefsBckup,
        rowData:[...this.state.rowDataBckup, ...selectedData],
        addMoreFlag: false,

      })
    }
  }

  updateStatus = () => {
    this.setState({ progress: true})
    Axios.post("http://localhost:8080/openDemand/updateStatus",
        this.state.rowData.map(row => ({ ...row, status: "Active" }))

    ).then(res =>{
      this.setState({progress: false})
      this.closeGrouping({show: true, message: `Successfully changed status to 'Active'`, type:"success"});
    })
    .catch(err => this.closeGrouping({show: true, message: err.message, type:"error"}))
  }

  ActiveStatusButton = () => {
    return <>
      { this.props.updateFlag
        ? this.state.addMoreFlag
          ? <Button onClick={this.addMoreToGroup} variant="outlined">
              {this.state.progress ? <CircularProgress size={25}/> : ''} Add
            </Button>
         :
        <>
          <Button onClick={this.addMoreToGroup} variant="outlined">
            Add more
          </Button>
          <Button onClick={this.updateDemandGroup} variant="outlined">
            {this.state.progress ? <CircularProgress size={25}/> : ''} Update
          </Button>
        </>
        : this.props.actionType === "changestatus"
          ? <Button onClick={this.updateHiringStatus} variant="outlined">
              {this.state.progress ? <CircularProgress size={25}/> : ''} Update
            </Button>
          :
          <Button onClick={this.createGroup} variant="outlined">
          {this.state.progress ? <CircularProgress size={25}/> : ''} Create
          </Button>
      }

        </>
  }

  OpenStatusButton = () => {
    return <>
            <Button onClick={this.updateStatus} variant="outlined">
              {this.state.progress ? <CircularProgress size={25}/> : ''} Update
            </Button>
          </>
  }

  handleChange = (event) => {
    this.setState({ hiringStatus: event.target.value })
  };

  render() {
    return (
      <Grid container spacing={1}>
        { this.props.actionType === "changestatus"
        ?
        <Grid item xs={5}>
            <FormControl style={{width:"100%"}}>
              <InputLabel htmlFor="status-native-simple">Hiring Status</InputLabel>
              <Select
                native
                value={this.state.hiringStatus}
                onChange={this.handleChange}
                inputProps={{
                  name: 'hiring-status',
                  id: 'status-native-simple',
                }}
              >
                <option value="hold">Hold</option>
                <option value="ongoing">On Going</option>
                <option value="cancelled">Cancelled</option>
                <option value="offered">Offered</option>
                <option value="fullfilled">Full Filled</option>
              </Select>
            </FormControl>
          </Grid>
        :
        ""
         }

          <Grid item xs={12}>
            <div className="ag-theme-alpine" style={ {height: '300px', width: '100%'} }>
                <AgGridReact
                    columnDefs={this.state.columnDefs}
                    rowData={this.state.rowData}
                    onGridReady={ params => this.gridApi = params.api }
                    rowSelection="multiple"
                    onFirstDataRendered={this.onFirstDataRendered.bind(this)}
                    context={this.state.context}
                    frameworkComponents={this.state.frameworkComponents}
                    >
                </AgGridReact>
            </div>
          </Grid>
          <Grid item xs={12} style={{textAlign: "right"}}>
            { this.props.status === "open" ? this.OpenStatusButton()
            : this.props.status === "active" ? this.ActiveStatusButton()
            : this.props.status === "inactive" ? "" : ""}
            <Button onClick={this.closeGrouping} variant="outlined">Cancel</Button>
          </Grid>
      </Grid>
    );
  }
}

export default JDGroupConfirmationTable;