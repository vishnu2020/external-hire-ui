import React, { Component } from 'react';
import { saveAs } from 'file-saver';
import { Grid, Paper, Typography, Button } from '@material-ui/core';
import CommonMultiSelect from '../../Common/CommonMultiSelect';
import { AgGridReact } from 'ag-grid-react';
import HistoryIconRenderer from './history/icons/HistoryIconRender';
import ShowCandidateDemandTable from './ShowCandidateDemandTable';

import 'ag-grid-community/dist/styles/ag-grid.css';
import 'ag-grid-community/dist/styles/ag-theme-alpine.css';
import Axios from 'axios';
import CommonModal from '../../Common/CommonModal';

export default class CandidateMapping extends Component {

    constructor(props) {
        super(props);
        this.state = {
            open: false,
            columnDefs: [
                // valueGetter: b137ValueGetter,
                { headerName: "Action", cellRenderer: "historyIcon", width: 100 },
                { headerName: "Request Id", field: "requestId", sortable: true, filter: true },
                { headerName: "Status", field: "status", sortable: true, filter: true },
                { headerName: "Hiring Status", field: "hiringStatus", sortable: true, filter: true },
                { headerName: "ID", field: "id", sortable: true, filter: true },
                { headerName: "Role", field: "role" , filter: true},
                { headerName: "Acc Name", field: "accountName", sortable: true, filter: true},
                { headerName: "Opportunity", field: "opportunity" },
                { headerName: "BO/Project", field: "boProject" },
                { headerName: "Experience", field: "experience", sortable: true, filter: true},
                { headerName: "Location", field: "location" , filter: true},
                { headerName: "Request Category", field: "requestCategory" },
                { headerName: "Requested By", field: "requestedBy" },
                { headerName: "Details", field: "details", resizable: true},
                { headerName: "Skills", field: "skills", filter: true, resizable: true },
                { headerName: "Requester Id", field: "requesterId" },
                ],
            context: { componentParent: this },
            frameworkComponents: { historyIcon: HistoryIconRenderer },
            rowData: [],
            gridApi: null,
            selectedData:[{name: 'Srigar', id: 1},{name: 'Sam', id: 2},
      {name: 'Srigar1', id: 3},{name: 'Sam1', id: 4},
      {name: 'Srigar2', id: 5},{name: 'Sam2', id: 6},{name: 'Sam3', id: 7},{name: 'Srigar4', id: 8},{name: 'Sam5', id: 9}]
        }
    }

    methodFromParent = cell => {
        console.log(cell)
        this.setState({
          open: true,
          selectedData: cell.candidateModel
        })
      };

      closeGrouping = (msg) => {
        this.refs.agGrid.api.deselectAll()
        this.setState({
          open: false
        })
        this.props.setNotificationToState(msg)
      }
      cancelAssignment = () => {
        this.setState({
            open: false
          })
      }

      fetchAllActiveDemands = () => {
        Axios.get("http://localhost:8080/openDemand/activeDemands").then(res => this.setState({rowData: res.data})).catch(err => console.log(err))
    }

    componentDidMount() {
        this.fetchAllActiveDemands()
    }

    exportAsExcel = () => {
        const selectedNodes = this.gridApi.getSelectedNodes();
        if(selectedNodes.length === 0){
        this.setState({
            isEmptyJobSelection: true,
            errorMessage: "Select atleast one demand to proceed"
            })
            return;
        }
        const selectedData = selectedNodes.map( node => node.data )
        Axios.post("http://localhost:8080/exportCandidateDemandExcel", selectedData, { responseType: 'arraybuffer' }).then((response) => {
            console.log(response);
            var blob = new Blob([response.data], { type: 'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet' });
            saveAs(blob, 'report.xlsx');
          }).catch(err => console.log(err))
        
    }

    render() {
        return (
            <>
                <CommonModal open={this.state.open}>
                    <Paper style={{padding:"40px"}} elevation={3}>
                    <Typography variant="h6" gutterBottom>
                        Mapped Candidates
                    </Typography>
                        <ShowCandidateDemandTable selectedData={this.state.selectedData}/>
                        <Grid container>
                            <Grid style={{textAlign:"right"}} item xs={12}>
                            <Button variant="outlined" onClick={this.cancelAssignment}>
                                Cancel
                            </Button>
                            </Grid>
                        </Grid>
                    </Paper>
                </CommonModal>
                <Grid container spacing={1}>
                    {/* <Grid item xs={3}>
                        <CommonMultiSelect/>
                    </Grid> */}
                    <Grid item xs={12}>
                        <div className="ag-theme-alpine" style={ {height: '500px', width: '100%'} }>
                            <AgGridReact
                                columnDefs={this.state.columnDefs}
                                rowData={this.state.rowData}
                                onGridReady={ params => this.gridApi = params.api }
                                context={this.state.context}
                                frameworkComponents={this.state.frameworkComponents}
                                rowSelection="multiple"
                                >
                            </AgGridReact>
                        </div>
                    </Grid>
                    <Grid style={{textAlign:"right"}} item xs={12}>
                        <Button onClick={this.exportAsExcel} variant="contained" color="primary">Export</Button>
                        <Button style={{marginLeft:"10px"}} variant="contained" color="primary">Import</Button>
                    </Grid>
                </Grid>
            </>
        )
    }
}

