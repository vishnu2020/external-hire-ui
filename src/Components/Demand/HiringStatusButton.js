import React from 'react';
import ToggleButton from '@material-ui/lab/ToggleButton';
import ToggleButtonGroup from '@material-ui/lab/ToggleButtonGroup';

export default function HiringStatusButton(props) {
  const [alignment, setAlignment] = React.useState('ongoing');

  const handleAlignment = (event, hiringStatus) => {
    setAlignment(hiringStatus);
    // console.log(hiringStatus);
    props.filterBasedOnHiringStatus(hiringStatus);
  };

  return (
    <ToggleButtonGroup
      value={alignment}
      exclusive
      onChange={handleAlignment}
      aria-label="text alignment"
      size="small"
    >
      <ToggleButton value="ongoing" aria-label="left aligned">
        {/* <FormatAlignLeftIcon /> */}
        Ongoing
      </ToggleButton>
      <ToggleButton value="onhold" aria-label="centered">
        {/* <FormatAlignCenterIcon /> */}
        Onhold
      </ToggleButton>
      <ToggleButton value="offered" aria-label="right aligned">
        {/* <FormatAlignRightIcon /> */}
        Offerd
      </ToggleButton>
      {/* <ToggleButton value="justify" aria-label="justified">
        <FormatAlignJustifyIcon />
      </ToggleButton> */}
    </ToggleButtonGroup>
  );
}
