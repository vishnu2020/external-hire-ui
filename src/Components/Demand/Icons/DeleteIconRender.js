import React, { Component } from 'react';
import {Button} from '@material-ui/core';
import DeleteIcon from '@material-ui/icons/Delete';

export default class DeleteIconRender extends Component {
  constructor(props) {
    super(props);
  }

  invokeParentMethod = () => {
    this.props.context.componentParent.methodFromParent(
      this.props.data
    );
  }

  render() {
    return (
      <Button onClick={this.invokeParentMethod}><DeleteIcon color="error"/></Button>
    );
  }
}