import React, {Component} from 'react';
import { AgGridReact } from 'ag-grid-react';

import 'ag-grid-community/dist/styles/ag-grid.css';
import 'ag-grid-community/dist/styles/ag-theme-alpine.css';
import Axios from 'axios';
import { Button, Grid, Paper, Input, CircularProgress } from '@material-ui/core';
import { CloudUploadOutlined, CloseOutlined } from '@material-ui/icons';
import {TextField, Typography, FormControlLabel, Checkbox} from '@material-ui/core';
import SelectRecruiter from '../../Common/Select';
import CommonModal from '../../Common/CommonModal';
import JDGroupConfirmationTable from './JDGroupConfirmationTable';
import SwitchActiveDemands from './SwitchActiveDemands';
import HistoryIconRenderer from './history/icons/HistoryIconRender'
import demandStyle from './css/demands.css'
import HiringStatusButton from './HiringStatusButton';

export default class ActiveDemand extends Component {
  constructor(props) {
    super(props);
    this.state = {
      gridApi: null,
      open:false,
      selectedData:null,
      progress:false,
      fileUploadProgress:false,
      isEmptyJobSelection: false,
      errorMessage: "",
      feeder:[],
      demandColumnDefs: [
        { headerName: "Request Id", checkboxSelection: true, field: "requestId", sortable: true, filter: true },
        { headerName: "Status", field: "status", sortable: true, filter: true },
        { headerName: "Hiring Status", field: "hiringStatus", sortable: true, filter: true },
        { headerName: "ID", field: "id", sortable: true, filter: true },
        { headerName: "Role", field: "role" , filter: true},
        { headerName: "Acc Name", field: "accountName", sortable: true, filter: true},
        { headerName: "Opportunity", field: "opportunity" },
        { headerName: "BO/Project", field: "boProject" },
        { headerName: "Experience", field: "experience", sortable: true, filter: true},
        { headerName: "Location", field: "location" , filter: true},
        { headerName: "Request Category", field: "requestCategory" },
        { headerName: "Requested By", field: "requestedBy" },
        { headerName: "Details", field: "details", resizable: true},
        { headerName: "Skills", field: "skills", filter: true, resizable: true },
        { headerName: "Requester Id", field: "requesterId" }
        ],
      demandRowData: [],
      groupDemandColumnDefs:[
        {headerName: "Group Name",field: "jdGroupName", sortable: true, filter: true },
        {headerName: "Created on",field: "createdDate", width:400, sortable: true, filter: true },
        {headerName: "Status",field: "status", width:150, sortable: true, filter: true },
        {headerName: "Hiring Status",field: "hiringStatus", width:200, sortable: true, filter: true },
        {headerName: "Action", cellRenderer: "historyIcon", width: 100 }
      ],
      groupDemandRowData:[],
      backUpGroupDemandRowData:[],
      switch: false,
      context: { componentParent: this },
      frameworkComponents: { historyIcon: HistoryIconRenderer },
      groupUpdateFlag: false,
      noHiringFlag: "ongoing",
      overlayNoRowsTemplate:
        `<span style="padding: 10px; background: lightgoldenrodyellow;">No active demands found</span>`,
      buttonType: "",
      selectedGroup: {},    
    }
  }

  componentDidMount() {
    this.setState({demandRowData: this.props.data.filter(row=> 
      row.status.toLowerCase() === 'active' && !row.grouped && row.hiringStatus === "ongoing")})
  }

  setNotificationMessage = (notification) => {
    this.props.setNotificationToState(notification)
  }

  emptySelectionValidation = () => {
    this.setState({
      isEmptyJobSelection: true,
      errorMessage: "Select atleast one demand to proceed"
    })
  }

  mapJD = (event) => {
    const selectedNodes = this.gridApi.getSelectedNodes()
    if(selectedNodes.length === 0){
      this.emptySelectionValidation();
      return;
    }
    const selectedData = selectedNodes.map( node => node.data )
    this.setState({ selectedData: selectedData, open:true, buttonType: event.target.innerText.replace(/ /g, "").toLowerCase()})
  }

  setRecruiter = (data) => {
    this.setState({
      recruiter:data,
      isRecruiterError: false,
      errorMessage: ""
    })
  }


  closeGrouping = (msg) => {
    if(this.state.switch)
      this.refs.agGrid.api.deselectAll()
    this.setState({
      open: false
    })
    this.props.setNotificationToState(msg)
  }

  rowSelected = () => {
    this.setState({
      isEmptyJobSelection: false,
      errorMessage: ""
    })
  }

  fetchDemandGroup = (flag) => {
    if(flag)
    {
      this.setState({ isEmptyJobSelection: false, errorMessage:"" })
      Axios.get("http://localhost:8080/openDemand/groupDemands?status=Active")
      .then(res => this.setState({switch: true, groupUpdateFlag:true, 
        groupDemandRowData: res.data.filter(row => row.hiringStatus === "ongoing"), backUpGroupDemandRowData : res.data}))
      .catch(err => console.log(err))
    }else{
      this.setState({switch: false, groupUpdateFlag: false})
    }
  }

  onFirstDataRendered = params => {
    if(this.state.switch)
      params.api.sizeColumnsToFit();
  };

  methodFromParent = cell => {
    this.setState({
      open: true,
      selectedData: cell.demands,
      selectedGroup: cell
    })
  };

  filterBasedOnHiringStatus = (status) => {
    if(this.state.switch) {
      if(status === "ongoing")
        this.setState({groupDemandRowData : this.state.backUpGroupDemandRowData.filter(demand => demand.status.toLowerCase() === 'active' && demand.hiringStatus === "ongoing")})
      else if(status === "offered")
        this.setState({groupDemandRowData : this.state.backUpGroupDemandRowData.filter(demand => demand.status.toLowerCase() === 'active' && demand.hiringStatus === "offered")})
      else if(status === "onhold")
        this.setState({groupDemandRowData : this.state.backUpGroupDemandRowData.filter(demand => demand.status.toLowerCase() === 'active' && demand.hiringStatus === "hold")})
    }else {
      if(status === "ongoing")
        this.setState({demandRowData : this.props.data.filter(demand => demand.status.toLowerCase() === 'active' && !demand.grouped && demand.hiringStatus === "ongoing")})
      else if(status === "offered")
        this.setState({demandRowData : this.props.data.filter(demand => demand.status.toLowerCase() === 'active' && !demand.grouped && demand.hiringStatus === "offered")})
      else if(status === "onhold")
        this.setState({demandRowData : this.props.data.filter(demand => demand.status.toLowerCase() === 'active' && !demand.grouped && demand.hiringStatus === "hold")})
    }
  }

  render() {
    return (
      <>
      <CommonModal style={{width:"100%"}} open={this.state.open}>
        <Paper style={{padding:"40px"}} elevation={3}>
          <Typography variant="h6" gutterBottom>
            Confirm
          </Typography>
          <JDGroupConfirmationTable 
            closeGrouping={this.closeGrouping} 
            openDemands={this.state.demandRowData} 
            openDemandsHeader={this.state.demandColumnDefs} 
            updateFlag={this.state.groupUpdateFlag} 
            selectedData={this.state.selectedData}
            status="active"
            actionType={this.state.buttonType}
            selectedGroup={ this.state.selectedGroup }
          />
        </Paper>
      </CommonModal>
      <Grid container spacing={1}>
        <Grid item xs={4}>
          <SwitchActiveDemands showDemandGroup={this.fetchDemandGroup} />
        </Grid>
        <Grid item xs={8}>
          <HiringStatusButton filterBasedOnHiringStatus={this.filterBasedOnHiringStatus}/>
        </Grid>
        <Grid item xs={12}>
          <div className="ag-theme-alpine" style={ {height: '450px', width: '100%'} }>
            <AgGridReact
                onGridReady={ params => this.gridApi = params.api }
                rowSelection="multiple"
                columnDefs={this.state.switch ? this.state.groupDemandColumnDefs : this.state.demandColumnDefs}
                rowData={this.state.switch ? this.state.groupDemandRowData : this.state.demandRowData}
                animateRows
                ref="agGrid"
                onSelectionChanged={this.rowSelected}
                rowDataUpdated={this.onFirstDataRendered.bind(this)}
                context={this.state.context}
                frameworkComponents={this.state.frameworkComponents}
                overlayNoRowsTemplate={this.state.overlayNoRowsTemplate}
                >
            </AgGridReact>
          {this.state.isEmptyJobSelection ? <p style={{color:"red"}}>{this.state.errorMessage}</p> : ""}
          </div>
        </Grid>
        { localStorage.getItem("isAdmin") === "true" ? "" 
        :  
        <Grid item xs={12} style={{textAlign:"right"}}>
          <Button variant="contained" value="changeStatus" onClick={this.mapJD} style={{backgroundColor:"#00B4B4", color:"white"}}>
            Change Status
          </Button>
          { this.state.switch ? "" 
          :
          <Button variant="contained" value="createJd" onClick={this.mapJD} style={{backgroundColor:"#00B4B4",marginLeft:"10px", color:"white"}}>
            Create JD
          </Button> 
          }
          
        </Grid>
        }
      </Grid>
      </>
    );
  }
}
