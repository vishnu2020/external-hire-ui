import React, {Component} from 'react';
import { AgGridReact } from 'ag-grid-react';

import 'ag-grid-community/dist/styles/ag-grid.css';
import 'ag-grid-community/dist/styles/ag-theme-alpine.css';
import Axios from 'axios';
import { Button, Grid, Paper, Input, CircularProgress } from '@material-ui/core';
import { CloudUploadOutlined, CloseOutlined } from '@material-ui/icons';
import {TextField, Typography, FormControlLabel, Checkbox} from '@material-ui/core';
import SelectRecruiter from '../../Common/Select';
import CommonModal from '../../Common/CommonModal';
import JDGroupConfirmationTable from './JDGroupConfirmationTable';

export default class InactiveDemands extends Component {
  constructor(props) {
    super(props);
    this.state = {
      gridApi: null,
      open:false,
      selectedData:null,
      progress:false,
      fileUploadProgress:false,
      isEmptyJobSelection: false,
      errorMessage: "",
      columnDefs: [
        { headerName: "Request Id", checkboxSelection: true, field: "requestId", sortable: true, filter: true },
        { headerName: "Status", field: "status", sortable: true, filter: true },
        { headerName: "Hiring Status", field: "hiringStatus", sortable: true, filter: true },
        { headerName: "Role", field: "role" , filter: true},
        { headerName: "Acc Name", field: "accountName", sortable: true, filter: true},
        { headerName: "Opportunity", field: "opportunity" },
        { headerName: "BO/Project", field: "boProject" },
        { headerName: "Experience", field: "experience", sortable: true, filter: true},
        { headerName: "Location", field: "location" , filter: true},
        { headerName: "Request Category", field: "requestCategory" },
        { headerName: "Requested By", field: "requestedBy" },
        { headerName: "Details", field: "details", resizable: true},
        { headerName: "Skills", field: "skills", filter: true, resizable: true },
        { headerName: "Requester Id", field: "requesterId" }
        ],
      rowData: [],
      overlayNoRowsTemplate:
        `<span style="padding: 10px; background: lightgoldenrodyellow;">No inactive demands found</span>`,
    }
  }

  componentDidMount() {
    this.setState({rowData: this.props.data.filter(row=> row.status.toLowerCase() === 'inactive')})
  }

  setNotificationMessage = (notification) => {
    this.props.setNotificationToState(notification)
  }

  mapJD = () => {
    const selectedNodes = this.gridApi.getSelectedNodes()
    if(selectedNodes.length === 0){
      this.setState({
        isEmptyJobSelection: true,
        errorMessage: "Select atleast one demand to proceed"
      })
      return;
    }
    const selectedData = selectedNodes.map( node => node.data )
    this.setState({ selectedData: selectedData, open:true})
  }

  closeGrouping = (msg) => {
    this.refs.agGrid.api.deselectAll()
    this.setState({
      open: false
    })
    this.props.setNotificationToState(msg)
  }

  rowSelected = () => {
    this.setState({
      isEmptyJobSelection: false,
      errorMessage: ""
    })
  }

  render() {
    return (
      <>
      <CommonModal style={{width:"100%"}} open={this.state.open}>
        <Paper style={{padding:"40px"}} elevation={3}>
          <Typography variant="h6" gutterBottom>
            Confirm
          </Typography>
          <JDGroupConfirmationTable closeGrouping={this.closeGrouping} selectedData={this.state.selectedData}/>
          {/* <Grid container>
            <Grid item xs={12}>
              <Button disabled={this.state.progress} variant="outlined" onClick={this.confirmAssignment}>
                Confirm {this.state.progress ? <CircularProgress size={25}/> : ''}
              </Button>
              <Button disabled={this.state.progress} variant="outlined" onClick={this.cancelAssignment}>
                Cancel
              </Button>
            </Grid>
          </Grid> */}
        </Paper>
      </CommonModal>
      <Grid container spacing={1}>
        <Grid item xs={12}>
          <div className="ag-theme-alpine" style={ {height: '450px', width: '100%'} }>
            <AgGridReact
                onGridReady={ params => this.gridApi = params.api }
                rowSelection="multiple"
                columnDefs={this.state.columnDefs}
                rowData={this.state.rowData}
                animateRows
                ref="agGrid"
                onSelectionChanged={this.rowSelected}
                overlayNoRowsTemplate={this.state.overlayNoRowsTemplate}
                >
            </AgGridReact>
          {this.state.isEmptyJobSelection ? <p style={{color:"red"}}>{this.state.errorMessage}</p> : ""}
          </div>
        </Grid>
      </Grid>
      </>
    );
  }
}
