import React,{Component} from 'react'
import { AgGridReact } from 'ag-grid-react';

import 'ag-grid-community/dist/styles/ag-grid.css';
import 'ag-grid-community/dist/styles/ag-theme-alpine.css';

class ConfirmationTable extends Component {
  constructor(props) {
    super(props);
    console.log(props)
    this.state = {
      columnDefs: [
        { headerName: "Request Id", field: "requestId", filter: true },
        { headerName: "Experience", field: "experience", filter: true},
        { headerName: "Recruiter", field: "recruiterName" }],
      rowData: this.props.selectedData.map(data=>{
          return { requestId: data.requestId, experience: data.experience, recruiterName: this.props.recruiter.employeeName}
      })
    }
  }

  render() {
    return (
      <div className="ag-theme-alpine" style={ {height: '200px', width: '600px'} }>
        <AgGridReact
            columnDefs={this.state.columnDefs}
            rowData={this.state.rowData}>
        </AgGridReact>
      </div>
    );
  }
}

export default ConfirmationTable;