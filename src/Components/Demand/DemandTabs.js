import React, { Component } from 'react';
import Notifications from '../../Common/Notifications';
import { AppBar, Tabs, Tab, Button, CircularProgress, Box, Typography } from '@material-ui/core';
import { CloudUploadOutlined } from '@material-ui/icons';
import ActiveDemands from './ActiveDemand';
import AssignDemand from "./AssignDemand";
import OpenDemand from './OpenDemands';
import Axios from 'axios';
import InactiveDemands from './InactiveDemands';
import './css/demands.css'

export default class DemandTabs extends Component {
    constructor(props){
        super(props);
        this.state ={
            value: 0,
            rowData: [],
            fileUploadProgress: false,
            notification:{show: false, message: "", type:""},
            demandTabs: {}
        }
    }

    handleChange = (event, newValue) => {
        console.log(newValue);
        this.setState({value: newValue});
      };

    closeNotification = () => {
        this.setState({notification: {
          show:false,
          message: "",
          type:""
        }})
      }

    a11yProps = (index) => {
        return {
          id: `simple-tab-${index}`,
          'aria-controls': `simple-tabpanel-${index}`,
        };
      }

    setNotificationToState = (notification) => {
        this.setState({notification: notification})
        if(notification.show)
            this.fetchDemandsFromServer()
    }

    closeNotification = () => {
        this.setState({notification: {
          show:false,
          message: "",
          type:""
        }})
    }

    componentWillMount() {
        if(localStorage.getItem("isAdmin") === "true")
              this.setState({demandTabs: {
                assign: {
                    index:0
                },
                open: {
                    index: 1
                },
                active: {
                    index: 2
                },
                inactive: {
                    index: 3
                }
            }})
            else
            this.setState({demandTabs: {
                assign: {
                    index:-1
                },
                open: {
                    index: 0
                },
                active: {
                    index: 1
                },
                inactive: {
                    index: 2
                }
            }})
    }

    fetchDemandsFromServer = () => {
        Axios.get("http://localhost:8080/openDemand/demand")
        .then(res => this.setState({rowData: res.data}))
        .catch(err => console.log(err.message))
    }

    componentDidMount() {
        this.fetchDemandsFromServer()
    }

    selectFile = (e) => {
        // console.log(e.target.files[0])
        this.setState({
            fileUploadProgress: true
        })
        var formData = new FormData();
        formData.append('demand', e.target.files[0])
        Axios.post('http://localhost:8080/openDemand/csvUpload', formData, {
          headers: {
            'Content-Type': 'multipart/form-data'
          }
        })
        .then(() => {
            this.setState({
                fileUploadProgress: false,
                notification: { show: true, message: "Successfully uploaded excel file", type:"success"},
                value: 0
            })
            this.fetchDemandsFromServer()
        })
        .catch(err => this.setState({notification: { show: true, message: `${err.message}`, type:"error"}}))
      }


    render() {

        const { notification, demandTabs, fileUploadProgress, value } = this.state;

        const TabPanel = (props) => {
            const { children, value, index, ...other } = props;

            return (
              <div
                role="tabpanel"
                hidden={value !== index}
                id={`simple-tabpanel-${index}`}
                aria-labelledby={`simple-tab-${index}`}
                {...other}
              >
                {value === index && (
                  <Box p={3}>
                    <Typography>{children}</Typography>
                  </Box>
                )}
              </div>
            );
          }

          console.log(demandTabs)

        return (
            <>
                <Notifications message={notification.message} type={notification.type} open={notification.show} closeFn={this.closeNotification}/>
                <div style={{flexGrow: 1, backgroundColor: "white"}}>
                    <AppBar color="default" position="static">
                        <Tabs value={value} onChange={this.handleChange} aria-label="simple tabs example">
                            { localStorage.getItem('isAdmin') === 'true' ? <Tab label="Assign" {...this.a11yProps(demandTabs.assign.index)} /> : ""}
                            <Tab label="Open" {...this.a11yProps(demandTabs.open.index)} />
                            <Tab label="Active" {...this.a11yProps(demandTabs.active.index)} />
                            <Tab label="Inactive" {...this.a11yProps(demandTabs.inactive.index)} />
                            { localStorage.getItem("isAdmin") === "true" ?
                                <Button
                                variant="text"
                                component="label"
                                >
                                {fileUploadProgress ? <CircularProgress size={25}/> : <CloudUploadOutlined/>}
                                <div style={{paddingLeft:"5px"}}>Excel</div>
                                <input
                                    type="file"
                                    style={{ display: "none" }}
                                    onChange={this.selectFile}
                                />
                                </Button>
                                : ""
                            }

                        </Tabs>
                    </AppBar>
                    { localStorage.getItem("isAdmin") === "true" ?
                    <TabPanel value={value} index={demandTabs.assign.index}>
                        <AssignDemand setNotificationToState={this.setNotificationToState} data={this.state.rowData}/>
                    </TabPanel>
                    :"" }
                    <TabPanel value={value} index={demandTabs.open.index}>
                        <OpenDemand setNotificationToState={this.setNotificationToState} data={this.state.rowData}/>
                    </TabPanel>
                    <TabPanel value={value} index={demandTabs.active.index}>
                        <ActiveDemands setNotificationToState={this.setNotificationToState} data={this.state.rowData}/>
                    </TabPanel>
                    <TabPanel value={value} index={demandTabs.inactive.index}>
                        <InactiveDemands setNotificationToState={this.setNotificationToState} data={this.state.rowData}/>
                    </TabPanel>
                </div>
            </>
        )
    }
}
