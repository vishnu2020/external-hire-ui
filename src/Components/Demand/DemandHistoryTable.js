import React,{Component} from 'react'
import { AgGridReact } from 'ag-grid-react';

import 'ag-grid-community/dist/styles/ag-grid.css';
import 'ag-grid-community/dist/styles/ag-theme-alpine.css';

class DemandHistoryTable extends Component {
  constructor(props) {
    super(props);
    this.state = {
        columnDefs: [
            { headerName: "Request Id", checkboxSelection: true, field: "requestId", sortable: true, filter: true },
            { headerName: "Status", field: "status", sortable: true, filter: true },
            { headerName: "Role", field: "role" , filter: true},
            { headerName: "Acc Name", field: "accountName", sortable: true, filter: true},
            { headerName: "Opportunity", field: "opportunity" },
            { headerName: "BO/Project", field: "boProject" },
            { headerName: "Experience", field: "experience", sortable: true, filter: true},
            { headerName: "Location", field: "location" , filter: true},
            { headerName: "Request Category", field: "requestCategory" },
            { headerName: "Requested By", field: "requestedBy" },
            { headerName: "Details", field: "details", resizable: true},
            { headerName: "Skills", field: "skills", filter: true, resizable: true },
            { headerName: "Requester Id", field: "requesterId" }
            ],
      rowData: this.props.selectedData
    }
  }

  render() {
    return (
      <div className="ag-theme-alpine" style={ {height: '500px', width: '100%'} }>
        <AgGridReact
            columnDefs={this.state.columnDefs}
            rowData={this.state.rowData.demands}>
        </AgGridReact>
      </div>
    );
  }
}

export default DemandHistoryTable;