import React, {Component} from 'react';
import { AgGridReact } from 'ag-grid-react';

import 'ag-grid-community/dist/styles/ag-grid.css';
import 'ag-grid-community/dist/styles/ag-theme-alpine.css';
import Axios from 'axios';
import { Button, Grid, Paper, Input, CircularProgress } from '@material-ui/core';
import { CloudUploadOutlined, CloseOutlined } from '@material-ui/icons';
import {TextField, Typography, FormControlLabel, Checkbox} from '@material-ui/core';
import SelectRecruiter from '../../Common/Select';
import CommonModal from '../../Common/CommonModal';
import ConfirmationTable from './ConfirmationTable';

export default class Demand extends Component {
  constructor(props) {
    super(props);
    this.state = {
      gridApi: null,
      open:false,
      selectedData:null,
      progress:false,
      recruiter:null,
      fileUploadProgress:false,
      isRecruiterError: false,
      isEmptyJobSelection: false,
      errorMessage: "",
      feeder:[],
      columnDefs: [
        { headerName: "Request Id", checkboxSelection: true, field: "requestId", sortable: true, filter: true },
        { headerName: "Status", field: "status", sortable: true, filter: true },
        { headerName: "Role", field: "role" , filter: true},
        { headerName: "Acc Name", field: "accountName", sortable: true, filter: true},
        { headerName: "Opportunity", field: "opportunity" },
        { headerName: "BO/Project", field: "boProject" },
        { headerName: "Experience", field: "experience", sortable: true, filter: true},
        { headerName: "Location", field: "location" , filter: true},
        { headerName: "Request Category", field: "requestCategory" },
        { headerName: "Requested By", field: "requestedBy" },
        { headerName: "Details", field: "details", resizable: true},
        { headerName: "Skills", field: "skills", filter: true, resizable: true },
        { headerName: "Requester Id", field: "requesterId" },
        { headerName: "Assigned", field: "assigned" }
        ],
      rowData: null,
      overlayNoRowsTemplate:
        `<span style="padding: 10px; background: lightgoldenrodyellow;">No demands found</span>`
    }
  }

  componentDidMount() {
    this.fetchAllDemands();
  }

  fetchAllDemands = () => {
    Axios.all([
      Axios.get('http://localhost:8080/openDemand/recruiter')
    ])
    .then(responseArr => {
      this.setState({feeder: responseArr[0].data})
      
    }).catch(err => 
      this.setNotificationMessage({ show: true, 
        message: `${err.message}`,
        type:"error" }))
  }

  setNotificationMessage = (notification) => {
    this.props.setNotificationToState(notification)
  }

  onClickAssign = e => {
    if(this.state.recruiter === null)
    {
      this.setState({
        isRecruiterError: true,
        errorMessage: "Select a recruiter to proceed"
      })
      return;
    }
    const selectedNodes = this.gridApi.getSelectedNodes()
    if(selectedNodes.length === 0){
      this.setState({
        isEmptyJobSelection: true,
        errorMessage: "Select atleast one demand to proceed"
      })
      return;
    }
    const selectedData = selectedNodes.map( node => node.data )
    this.setState({ selectedData: selectedData, recruiter: this.state.recruiter, open:true})
  }

  setRecruiter = (data) => {
    this.setState({
      recruiter:data,
      isRecruiterError: false,
      errorMessage: ""
    })
  }

  confirmAssignment = () => {
    this.setState({
      progress: true
    })
    let selectedRecruiter = this.state.recruiter;
    selectedRecruiter.demands = this.state.selectedData;
    Axios.post("http://localhost:8080/openDemand/assign",
      selectedRecruiter
    ).then(assigned => {
      this.setState({progress:false, open:false});
      this.fetchAllDemands();
      this.setNotificationMessage({ show: true, 
        message: `${selectedRecruiter.employeeName} is assigned with ${selectedRecruiter.demands.length} demands`,
        type:"success" })
    }).catch(err => console.log(err))
  }

  cancelAssignment = () => {
    this.refs.agGrid.api.deselectAll()
    this.setState({
      open: false
    })
  }

  rowSelected = () => {
    this.setState({
      isEmptyJobSelection: false,
      errorMessage: ""
    })
  }

  render() {
    return (
      <>
      <CommonModal open={this.state.open}>
        <Paper style={{padding:"40px"}} elevation={3}>
          <Typography variant="h6" gutterBottom>
            Confirm
          </Typography>
          <ConfirmationTable selectedData={this.state.selectedData} recruiter={this.state.recruiter}/>
          <Grid container>
            <Grid item xs={12}>
              <Button disabled={this.state.progress} variant="outlined" onClick={this.confirmAssignment}>
                Confirm {this.state.progress ? <CircularProgress size={25}/> : ''}
              </Button>
              <Button disabled={this.state.progress} variant="outlined" onClick={this.cancelAssignment}>
                Cancel
              </Button>
            </Grid>
          </Grid>
        </Paper>
      </CommonModal>
      <Grid container spacing={1}>
        <Grid item xs={12}>
          <Grid style={{padding:"10px 10px"}} container>
            <Grid item xs={6}>
              <SelectRecruiter data={this.state.feeder}
              setRecruiter={this.setRecruiter}
              error={this.state.isRecruiterError}
              errorMsg={this.state.errorMessage}/>
            </Grid>
          </Grid>
        </Grid>
        <Grid item xs={12}>
          <div className="ag-theme-alpine" style={ {height: '450px', width: '100%'} }>
            <AgGridReact
                onGridReady={ params => this.gridApi = params.api }
                rowSelection="multiple"
                columnDefs={this.state.columnDefs}
                rowData={this.props.data.filter(demand=> demand.assigned === false)}
                animateRows
                ref="agGrid"
                onSelectionChanged={this.rowSelected}
                overlayNoRowsTemplate={this.state.overlayNoRowsTemplate}
                >
            </AgGridReact>
          {this.state.isEmptyJobSelection ? <p style={{color:"red"}}>{this.state.errorMessage}</p> : ""}
          </div>
        </Grid>
        <Grid item xs={12} style={{textAlign:"right"}}>
          <Button variant="contained" onClick={this.onClickAssign} style={{backgroundColor:"#00B4B4", color:"white"}}>
            Assign
          </Button>
        </Grid>
      </Grid>
      </>
    );
  }
}
